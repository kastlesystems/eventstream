﻿using System;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderImage Class for credential holder image
    /// </summary>
    [XmlRoot("CredentialHolderImage", Namespace = "urn:psialliance-org")]
    public class CredentialHolderImage
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the u identifier.
        /// </summary>
        [XmlElement("UID", Order = 1)]
        public string UId { get; set; }

        /// <summary>
        /// Gets or sets the image extension.
        /// </summary>
        [XmlElement("ImageExtension", Order = 2)]
        public string ImageExtension { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        [XmlElement("ImageData", Order = 3)]
        public string ImageData { get; set; }

        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("LastUpdateDate", Order = 4)]
        public DateTime LastUpdateDate { get; set; }
    }
}
