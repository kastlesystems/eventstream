﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderImage Class for credential holder image
    /// </summary>
    [XmlRoot("CredentialHolderCredentials", Namespace = "urn:psialliance-org")]
    public class CredentialHolderCredentials
    {
        /// <summary>
        /// Gets or sets the u identifier.
        /// </summary>
        [XmlElement("UID", Order = 1)]
        [Display(Name = "IdentityGuid")]
        public string UID { get; set; }

        /// <summary>
        /// Gets or sets the image extension.
        /// </summary>
        [XmlElement("EmployeeId", Order = 2)]
        [Display(Name = "EmployeeID")]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        [XmlElement("VirtualCredential", Order = 3)]
        [Display(Name = "VirtualCredential")]
        public string VirtualCredential { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        [XmlElement("PhysicalCredential", Order = 4)]
        //[Display(Name = "PhysicalCredential")]
        public string PhysicalCredential { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        [XmlElement("LastUpdate", Order = 5)]
        public DateTime LastUpdate { get; set; }

        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("CardHolderId", Order = 6)]
        public string CardHolderId { get; set; }


        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("NCCityCode", Order = 7)]
        public string NCCityCode{ get; set; }


        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("NCOfflineCode", Order = 8)]
        public string NCOfflineCode { get; set; }

        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("PhysicalCredentialHexa", Order = 9)]
        [Display(Name = "PhysicalCredential")]
        public string PhysicalCredentialHexa { get; set; }

        /// <summary>
        /// Gets or sets the last update date.
        /// </summary>
        [XmlElement("PLAICredentialFormatLength", Order = 10)]
        [Display(Name = "CredentialFormatLength")]
        public string PLAICredentialFormatLength { get; set; }
    }
}
