﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialState Class for credential state
    /// </summary>
    [XmlRoot("CredentialState", Namespace = "urn:psialliance-org")]
    public class CredentialState
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        [XmlElement("State")]
        public StateOfCredential State { get; set; }
    }
}
