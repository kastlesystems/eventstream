﻿using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.ResponseStatus Class for response status
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class ResponseStatus
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the request URL.
        /// </summary>
        [XmlElement(ElementName = "requestURL", DataType = "anyURI", Order = 0)]
        public string RequestURL { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        [XmlElement("statusCode", Order = 1)]
        public int StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the status string.
        /// </summary>
        [XmlElement("statusString", Order = 2)]
        public string StatusString { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("id", Order = 3)]
        public string Id { get; set; }


        [XmlElement("Extensions", Order = 4)]
        public List<ResponseStatusExt> Extensions { get; set; }

    }

    [XmlRoot("")]
    public class ResponseStatusExt
    {
        [XmlElement("ResponseStatusExt", Order = 0)]
        public XmlElement ResponseStatusExtXml;

    }



}
