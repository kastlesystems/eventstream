﻿using System;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.Time Class for time
    /// </summary>
    [XmlRoot("Time", Namespace = "urn:psialliance-org")]
    public class Time
    {
        /// <summary>
        /// version
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }

        /// <summary>
        /// timeMode
        /// </summary>
        [XmlElement("timeMode")]
        public string timeMode { get; set; }

        /// <summary>
        /// localTime
        /// </summary>
        [XmlElement("localTime")]
        public DateTime localTime { get; set; }

        /// <summary>
        /// timeZone
        /// </summary>
        [XmlElement("timeZone")]
        public string timeZone { get; set; }
    }
}
