﻿

using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    [XmlRoot("Institution", Namespace = "urn:psialliance-org")]
    public class Institution
    {
        [XmlAttribute("InstId")]
        public int InstId { get; set; }
        [XmlAttribute("InstitutionName")]
        public string InstitutionName { get; set; }
    }
}
