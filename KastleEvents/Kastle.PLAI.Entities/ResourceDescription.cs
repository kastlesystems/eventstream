﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.ResourceDescription Class for resource description
    /// </summary>
    [XmlRoot("ResourceDescription", Namespace = "urn:psialliance-org")]
    public class ResourceDescription
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name", Order = 0)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [XmlElement("type", Order = 1)]
        public ResourceType Type { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlElement("description", Order = 2)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        [XmlElement("notes", Order = 3)]
        public string Notes { get; set; }
    }
}
