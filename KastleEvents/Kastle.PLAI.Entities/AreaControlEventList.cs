﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class AreaControlEventList
    {
        /// <summary>
        /// Gets or sets the area control event information.
        /// </summary>
        [XmlElement("AreaControlEvent")]
        public List<AreaControlEvent> AreaControlEvent { get; set; }
    }
}