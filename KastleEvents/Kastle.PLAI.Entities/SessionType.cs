﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.SessionType Class for session type
    /// </summary>
    public class SessionType
    {
        /// <summary>
        /// Gets or sets the meta session protocol.
        /// </summary>
        [XmlElement("metaSessionProtocol")]
        public SessionProtocolType metaSessionProtocol { get; set; }

        /// <summary>
        /// Gets or sets the type of the meta session flow.
        /// </summary>
        /// <value>
        /// The type of the meta session flow.
        /// </value>
        [XmlElement("metaSessionFlowType")]
        public SessionFlowType metaSessionFlowType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [meta session flow type specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meta session flow type specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metaSessionFlowTypeSpecified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [meta session persistent].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meta session persistent]; otherwise, <c>false</c>.
        /// </value>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public bool metaSessionPersistent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [meta session persistent specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meta session persistent specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metaSessionPersistentSpecified { get; set; }
    }
}
