﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CardFormatInfoList Class for card format info list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CardFormatInfoList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("WiegandCardFormatInfo")]
        public List<WiegandCardFormatInfo> WiegandCardFormatInfo { get; set; }
    }
}
