﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PermissionIDList Class for permission identifier list
    /// </summary>
    public class PermissionIDList
    {
        /// <summary>
        /// Gets or sets the permission identifier.
        /// </summary>
        [XmlElement("PermissionID")]
        public List<PermissionID> PermissionID { get; set; }
    }

    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PermissionID Class for permission identifier
    /// </summary>
    public class PermissionID
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("ID")]
        public int ID { get; set; }
    }
}
