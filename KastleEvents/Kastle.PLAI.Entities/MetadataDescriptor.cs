﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetadataDescriptor Class for metadata descriptor
    /// </summary>
    public class MetadataDescriptor
    {
        /// <summary>
        /// Gets or sets the meta identifier.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public MetadataIDDescr metaID { get; set; }

        /// <summary>
        /// Gets or sets the meta type list.
        /// </summary>
        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified), XmlArrayItemAttribute("metadataType", typeof(MetadataTypeDescr), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public MetadataTypeDescr[][] metaTypeList { get; set; }
    }
}
