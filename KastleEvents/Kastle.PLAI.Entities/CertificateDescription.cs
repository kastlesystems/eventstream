﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CertificateDescription Class for certificate description
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CertificateDescription
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the certificate format.
        /// </summary>
        [XmlElement("CertificateFormat")]
        public string CertificateFormat { get; set; }

        /// <summary>
        /// Gets or sets the certificate text.
        /// </summary>
        [XmlElement("CertificateText")]
        public string CertificateText { get; set; }
    }
}
