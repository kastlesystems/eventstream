﻿namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.DeviceOwnershipRequest Class for device ownership request
    /// </summary>
    public class DeviceOwnershipRequest
    {
        /// <summary>
        /// Gets or sets the owner unique identifier.
        /// </summary>
        public string OwnerGUID { get; set; }

        /// <summary>
        /// Gets or sets the expire time.
        /// </summary>
        public string ExpireTime { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }
    }
}
