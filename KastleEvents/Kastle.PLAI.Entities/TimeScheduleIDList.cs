﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.TimeScheduleIDList Class for time schedule identifier list
    /// </summary>
    public class TimeScheduleIDList
    {
        /// <summary>
        /// Gets or sets the time schedule identifier.
        /// </summary>
        [XmlElement("TimeScheduleID")]
        public List<ReferenceID> TimeScheduleID { get; set; }
    }
}
