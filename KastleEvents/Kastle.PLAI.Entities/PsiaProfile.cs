﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PsiaProfile Class for psia profile
    /// </summary>
    [XmlRoot("PsiaProfile", Namespace = "urn:psialliance-org")]
    public class PsiaProfile
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the system identifier.
        /// </summary>
        [XmlElement("systemID", Order = 0)]
        public string SystemID { get; set; }

        /// <summary>
        /// Gets or sets the native identifier.
        /// </summary>
        [XmlElement("nativeID", Order = 1)]
        public string NativeID { get; set; }

        /// <summary>
        /// Gets or sets the psia service version.
        /// </summary>
        [XmlElement("psiaServiceVersion", Order = 2)]
        public float PsiaServiceVersion { get; set; }

        /// <summary>
        /// Gets or sets the primary psia spec.
        /// </summary>
        [XmlElement("primaryPsiaSpec", Order = 3)]
        public PsiaSpecDecl PrimaryPsiaSpec { get; set; }

        /// <summary>
        /// Gets or sets the other spec list.
        /// </summary>
		[XmlIgnore()]
        [XmlArray("otherSpecList", Order = 4)]
        [XmlArrayItem("psiaSpecDefn")]
        public List<PsiaSpecDecl> OtherSpecList { get; set; }

        /// <summary>
        /// Gets or sets the profile list.
        /// </summary>
        [XmlArray("profileList", Order = 5)]
        [XmlArrayItem("psiaProfileDefn")]
        public List<PsiaProfileDefn> ProfileList { get; set; }

        /// <summary>
        /// Gets or sets the node description.
        /// </summary>
        [XmlElement("nodeDescription", Order = 6)]
        public string NodeDescription { get; set; }

    }
}
