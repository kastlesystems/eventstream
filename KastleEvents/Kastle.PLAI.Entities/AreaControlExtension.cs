﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    public class AreaControlExtension
    {
        /// <summary>
        /// Gets or sets the name of the custom extension.
        /// </summary>
        /// <value>
        /// The name of the custom extension.
        /// </value>
        [XmlElement(DataType = "anyURI")]
        public string CustomExtensionName { get; set; }

        /// <summary>
        /// Gets or sets any.
        /// </summary>
        [XmlAnyElementAttribute()]
        public XmlElement[] Any { get; set; }
    }
}
