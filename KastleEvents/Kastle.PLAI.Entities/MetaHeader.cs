﻿using System;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    public class MetaHeader
    {
        /// <summary>
        /// Gets or sets the meta version.
        /// </summary>
        public float MetaVersion { get; set; }

        /// <summary>
        /// Gets or sets the meta identifier.
        /// </summary>
        [XmlElement(DataType = "anyURI")]
        public string MetaID { get; set; }

        /// <summary>
        /// Gets or sets the meta source identifier.
        /// </summary>
        public string MetaSourceID { get; set; }

        /// <summary>
        /// Gets or sets the meta source local identifier.
        /// </summary>
        public uint MetaSourceLocalID { get; set; }

        /// <summary>
        /// Gets or sets the meta time.
        /// </summary>
        public DateTime MetaTime { get; set; }

        /// <summary>
        /// Gets or sets the meta priority.
        /// </summary>
        public byte MetaPriority { get; set; }

        /// <summary>
        /// Gets or sets the meta link.
        /// </summary>
        public string MetaLink { get; set; }

        /// <summary>
        /// Gets or sets the meta HDR extension.
        /// </summary>
        public HdrExtension MetaHdrExtension { get; set; }
    }
}
