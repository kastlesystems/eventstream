﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialInfoList Class for credential information list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CredentialInfoList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the credential information.
        /// </summary>
        [XmlElement("CredentialInfo", Order = 0)]
        public List<CredentialInfo> CredentialInfo { get; set; }
    }
}
