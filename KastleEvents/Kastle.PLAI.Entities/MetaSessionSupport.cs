﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    [XmlRoot("MetaSessionSupport", Namespace = "urn:psialliance-org")]
    public class MetaSessionSupport
    {
        /// <summary>
        /// Gets or sets the meta formats.
        /// </summary>
        [XmlArray("metaFormats")]
        [XmlArrayItemAttribute("metaFormat")]
        public MetaFormat[] metaFormats { get; set; }

        /// <summary>
        /// Gets or sets the meta session types.
        /// </summary>
        [XmlArray("metaSessionTypes")]
        [XmlArrayItemAttribute("metaSessionType")]
        public SessionType[] metaSessionTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [multicast capable].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [multicast capable]; otherwise, <c>false</c>.
        /// </value>
        [XmlElementAttribute]
        public bool multicastCapable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [schedule capable].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [schedule capable]; otherwise, <c>false</c>.
        /// </value>
        [XmlElementAttribute]
        public bool scheduleCapable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [query parms supported].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [query parms supported]; otherwise, <c>false</c>.
        /// </value>
        [XmlElementAttribute]
        public bool queryParmsSupported { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [query parms supported specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [query parms supported specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool queryParmsSupportedSpecified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [receives metadata].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [receives metadata]; otherwise, <c>false</c>.
        /// </value>
        [XmlElementAttribute]
        public bool receivesMetadata { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [receives metadata specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [receives metadata specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool receivesMetadataSpecified { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }
    }
}
