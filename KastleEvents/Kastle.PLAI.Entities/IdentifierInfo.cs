﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.IdentifierInfo Class for identifier information
    /// </summary>
    [XmlRoot("IdentifierInfo", Namespace = "urn:psialliance-org")]
    [XmlInclude(typeof(IdentifierCardComponentInfo))]
    public class IdentifierInfo
    {
        /// <summary>
        /// Gets or sets the type of the identifier.
        /// </summary>
        /// <value>
        /// The type of the identifier.
        /// </value>
        [XmlElement("Type", Order = 0)]
        public IdentifierType IdentifierType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlElement("Value", Order = 1)]
        public string Value { get; set; }

        /// <summary>
        /// Determines whether Value is specified in the XML data.
        /// </summary>
        [XmlIgnore()]
        public bool ValueSpecified { get; set; }

        /// <summary>
        /// Gets or sets the value encoding.
        /// </summary>
        [XmlElement("ValueEncoding", Order = 2)]
        public IdentifierInfoValueEncoding ValueEncoding { get; set; }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        [XmlElement("Format", Order = 3)]
        public ReferenceID Format { get; set; }

        /// <summary>
        /// Used to store the card data for Raw Bits implementation.
        /// </summary>
        [XmlElement("RawValue", Order = 4)]
        public string RawValue { get; set; }

        /// <summary>
        /// Determines whether RawValue is specified in the XML data.
        /// </summary>
        [XmlIgnore()]
        public bool RawValueSpecified { get; set; }

        /// <summary>
        /// Gets or sets the card component list.
        /// </summary>
        [XmlArray("CardComponentList", Order = 5)]
        [XmlArrayItem("IdentifierCardComponentInfo")]
        public List<IdentifierCardComponentInfo> CardComponentList { get; set; }

        /// <summary>
        /// Determines whether CardComponentList is specified in the XML data.
        /// This is not specified for Raw Bits.
        /// </summary>
        [XmlIgnore()]
        public bool CardComponentListSpecified { get; set; }
    }

    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.IdentifierCardComponentInfo Class for identifier card component information
    /// </summary>
    [XmlType("IdentifierCardComponentInfo")]
    public class IdentifierCardComponentInfo
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [XmlElement("Type", Order = 0)]
        public CardFormatDataFieldType Type { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlElement("Value", Order = 1)]
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the value encoding.
        /// </summary>
        [XmlElement("ValueEncoding", Order = 2)]
        public IdentifierInfoValueEncoding ValueEncoding { get; set; }
    }
}
