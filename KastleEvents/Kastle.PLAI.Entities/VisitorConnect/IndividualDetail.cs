﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    [DataContract]
    [Serializable]
    public class IndividualDetail
    {
        [DataMember]
        public int ID
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [DataMember]
        public string LastName
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [DataMember]
        public List<string> FirstNames
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the first names.
        /// </summary>
        /// <value>The first names.</value>
        [DataMember]
        public string Information
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the information.
        /// </summary>
        /// <value>The information.</value>
        [DataMember]
        public string Address
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        [DataMember]
        public string Action
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        [DataMember]
        public string EmailToNotify
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the email to notify.
        /// </summary>
        /// <value>The email to notify.</value>
        [DataMember]
        public CompanySummary CompanySummary
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the company summary.
        /// </summary>
        /// <value>The company summary.</value>
        [DataMember]
        public ImageInfo ImageData
        {
            get;
            set;
        }

        [DataMember]
        public int VisitorId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsWatchListNotificationEnabled
        {
            get;
            set;
        }

        [DataMember]
        public bool IsExactMatchFound
        {
            get;
            set;
        }

    }
}
