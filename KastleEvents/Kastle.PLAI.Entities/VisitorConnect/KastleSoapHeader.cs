﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Class KastleSoapHeader.
    /// </summary>
    [DataContract]
    [Serializable]
    public class KastleSoapHeader
    {
        #region "class public Properties"

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        [DataMember]
        // property UserID
        public string UserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user role.
        /// </summary>
        /// <value>The user role.</value>
        [DataMember]
        // property UserRole
        public string UserRole
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company identifier.
        /// </summary>
        /// <value>The company identifier.</value>
        [DataMember]
        // property CompanyID
        public int CompanyID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the cardholder identifier.
        /// </summary>
        /// <value>The cardholder identifier.</value>
        [DataMember]
        // property CardholderID
        public int CardholderID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the super user identifier.
        /// </summary>
        /// <value>The super user identifier.</value>
        [DataMember]
        // property SuperUserID
        public string SuperUserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the super user card holder identifier.
        /// </summary>
        /// <value>The super user card holder identifier.</value>
        [DataMember]
        // property SuperUserCardHolderID
        public int SuperUserCardHolderID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the super user card identifier.
        /// </summary>
        /// <value>The super user card identifier.</value>
        [DataMember]
        // property SuperUserCardID
        public int SuperUserCardID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the first name of the authorizer.
        /// </summary>
        /// <value>The first name of the authorizer.</value>
        [DataMember]
        public string AuthorizerFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last name of the authorizer.
        /// </summary>
        /// <value>The last name of the authorizer.</value>
        [DataMember]
        public string AuthorizerLastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        /// <value>The date format.</value>
        [DataMember]
        public string DateFormat
        {
            get;
            set;
        }


        //NM - 6Nov2012 - for Change confirmation - adding sugar case number & notification flag properties        
        /// <summary>
        /// Gets or sets the sugar case.
        /// </summary>
        /// <value>The sugar case.</value>
        [DataMember]
        public string SugarCase { get; set; }

        /// <summary>
        /// Gets or sets the notification flag.
        /// </summary>
        /// <value>The notification flag.</value>
        [DataMember]
        public int NotificationFlag { get; set; }

        /// <summary>
        /// Gets or sets the e mail address.
        /// </summary>
        /// <value>The e mail address.</value>
        [DataMember]
        public string EMailAddress { get; set; }

        [DataMember]
        public string SuperUserName { get; set; }

        #endregion
    }
}
