﻿using System.Runtime.Serialization;
using System;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Enum AccessProfileType
    /// </summary>
    [DataContract]
    [Serializable]
    public enum AccessProfileType
    {

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] Default = 0,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] Hybrid = 1,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] AccessProfileOnly = 2

    }
}