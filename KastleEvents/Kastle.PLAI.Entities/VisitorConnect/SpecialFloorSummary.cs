﻿using System.Runtime.Serialization;
using System;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Class SpecialFloorSummary.
    /// </summary>
    [DataContract]
    [Serializable]
    public class SpecialFloorSummary
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [DataMember]
        public string ID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember]
        public string Description
        {
            get;
            set;
        }
    }
}
