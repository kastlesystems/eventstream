﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Kastle.PLAI.Entities.VisitorConnect
{
    public class EmailDetailsDalResponse : EmailBase
    {
        public Status Status { get; set; }

        /// <summary>
        /// Gets or sets to.
        /// </summary>
        /// <value>To.</value>
        public List<string> To { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }
        /// <summary>
        /// Gets or sets the cc.
        /// </summary>
        /// <value>The cc.</value>
        public List<string> CC { get; set; }

        /// <summary>
        /// Gets or sets the BCC.
        /// </summary>
        /// <value>The BCC.</value>
        public List<string> BCC { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>The body.</value>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets from.
        /// </summary>
        /// <value>From.</value>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the body format.
        /// </summary>
        /// <value>The body format.</value>
        public BodyFormat BodyFormat { get; set; }

        /// <summary>
        /// The _ attachment file name
        /// </summary>
        private List<string> _AttachmentFileName;

        /// <summary>
        /// Gets or sets the name of the attachment file.
        /// </summary>
        /// <value>The name of the attachment file.</value>
        public List<string> AttachmentFileName
        {
            get { return _AttachmentFileName; }
            set { _AttachmentFileName = value; }
        }

        /// <summary>
        /// Gets or sets the name of the displaye file.
        /// </summary>
        /// <value>The name of the displaye file.</value>
        public string DisplayeFileName { get; set; }

        /// <summary>
        /// Gets or sets the has attchment.
        /// </summary>
        /// <value>The has attchment.</value>
        public int HasAttchment { get; set; }
    }
}
