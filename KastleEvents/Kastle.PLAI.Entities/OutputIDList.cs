﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.OutputIDList Class for output identifier list
    /// </summary>
    public class OutputIDList
    {
        /// <summary>
        /// Gets or sets the output identifier.
        /// </summary>
        [XmlElement("OutputID")]
        public List<ReferenceID> OutputID { get; set; }
    }
}
