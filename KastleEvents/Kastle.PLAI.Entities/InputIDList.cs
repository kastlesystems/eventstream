﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.InputIDList Class for input identifier list
    /// </summary>
    public class InputIDList
    {
        /// <summary>
        /// Gets or sets the input identifier.
        /// </summary>
        [XmlElement("InputID")]
        public List<ReferenceID> InputID { get; set; }
    }
}
