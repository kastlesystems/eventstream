﻿using System;
using System.IO;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.StreamModel Class for stream model
    /// </summary>
    public class StreamModel
    {
        /// <summary>
        /// Gets or sets the stream writer.
        /// </summary>
        public StreamWriter StreamWriter { get; set; }
        /// <summary>
        /// Gets or sets the company identifier.
        /// </summary>
        public int CompanyId { get; set; }
        public int AuthorizerId { get; set; }
        public string UserName { get; set; }
        public string OwnerGuid { get; set; }
        /// <summary>
        /// Gets or sets the last event record time.
        /// </summary>
        public string LastEventRecTime { get; set; }
        public DateTime LastHeartbeatTime { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is agent call.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is agent call; otherwise, <c>false</c>.
        /// </value>
        public bool IsAgentCall { get; set; }
        public bool IsDemo { get; set; }
        public bool IsDemoEntryEvent { get; set; }
        public DateTime DemoLastEventTime { get; set; }
    }
}
