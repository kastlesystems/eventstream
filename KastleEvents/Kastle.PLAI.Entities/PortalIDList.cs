﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PortalIDList Class for portal identifier list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class PortalIDList
    {
        /// <summary>
        /// Gets or sets the portal identifier.
        /// </summary>
        [XmlElement("PortalID")]
        public List<ReferenceID> PortalID { get; set; }
    }
}
