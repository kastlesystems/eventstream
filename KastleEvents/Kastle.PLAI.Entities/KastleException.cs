﻿using System;
using System.Runtime.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.KastleException Class for kastle exception
    /// </summary>
    [Serializable]
    public class KastleException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KastleException"/> class.
        /// </summary>
        public KastleException()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KastleException"/> class.
        /// </summary>
        /// <param name="strMessage">The string message.</param>
        public KastleException(string strMessage)
            : base(strMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KastleException"/> class.
        /// </summary>
        /// <param name="strMessage">The string message.</param>
        /// <param name="ex">The ex.</param>
        public KastleException(string strMessage, Exception ex)
            : base(strMessage, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KastleException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected KastleException(SerializationInfo info, 
         StreamingContext context) : base(info, context)
        {
            
        }
    }
}
