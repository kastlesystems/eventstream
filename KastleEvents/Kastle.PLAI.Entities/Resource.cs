﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.Resource Class for resource
    /// </summary>
    public class Resource
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name", Order = 0)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the resource.
        /// </summary>
        /// <value>
        /// The type of the resource.
        /// </value>
        [XmlElement("type", Order = 1)]
        public ResourceType ResourceType { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlElement("description", Order = 2)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the resource list.
        /// </summary>
        [XmlElement("ResourceList", Order = 3)]
        public ResourceList ResourceList { get; set; }
    }
}
