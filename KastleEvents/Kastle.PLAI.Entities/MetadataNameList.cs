﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetadataNameList Class for metadata name list
    /// </summary>
    public class MetadataNameList
    {
        /// <summary>
        /// Gets or sets the metadata identifier string.
        /// </summary>
        [XmlElement("metadataIDString", Form = XmlSchemaForm.Unqualified, DataType = "anyURI")]
        public string[] metadataIDString { get; set; }
    }
}
