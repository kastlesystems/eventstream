﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialNumberList Class for credential number list
    /// </summary>
    public class CredentialNumberList
    {
        /// <summary>
        /// Gets or sets the credential number.
        /// </summary>
        [XmlElement("CredentialNumber")]
        public List<StringReference> CredentialNumber { get; set; }

        /// <summary>
        /// Gets or sets any attribute.
        /// </summary>
        [XmlAnyAttributeAttribute()]
        public XmlAttribute[] AnyAttr { get; set; }
    }
}
