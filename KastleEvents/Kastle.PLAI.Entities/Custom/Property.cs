﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class Property
    {
        [XmlElement("Value", Order = 0)]
        public string Value { get; set; }

        [XmlElement("PropertyName", Order = 1)]
        public string PropertyName { get; set; }

        [XmlElement("IsMandatory", Order = 2)]
        public bool IsMandatory { get; set; }

        [XmlElement("IsAux", Order = 3)]
        public bool IsAux { get; set; }
    }
}
