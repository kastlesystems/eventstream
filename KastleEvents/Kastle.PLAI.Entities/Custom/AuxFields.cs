﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kastle.PLAI.Entities.Custom
{
    public class AuxFields
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
