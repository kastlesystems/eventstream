﻿
namespace Kastle.PLAI.Entities
{
    public class LdapAttribute
    {
        public string LdapProperty { get; set; }

        public string LdapValue { get; set; }
    }
}
