﻿namespace Kastle.PLAI.Entities.Custom
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int CompanyId { get; set; }
        public bool IsAdmin { get; set; }
        public int PlaiType { get; set; }
        public int UserType { get; set; }
    }
}
