﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kastle.PLAI.Entities.Custom
{
    public class Personnel
    {
        public int PersonnelId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public List<CardDetails> Cards { get; set; }
    }
}
