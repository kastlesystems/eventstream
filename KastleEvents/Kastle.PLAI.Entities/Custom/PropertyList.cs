﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class PropertyList
    {
        [XmlElement("Property", Order = 0)]
        public List<Property> Property { get; set; }
    }
}
