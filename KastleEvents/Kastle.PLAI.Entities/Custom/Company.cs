﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kastle.PLAI.Entities.Custom
{
    public class Company
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int BuildingId { get; set; }
        public string BuildingNumber { get; set; }
        public string BuildingAddress { get; set; }
        public int HourOffSet { get; set; }
        public string TimeZoneDisplayName { get; set; }
        public string CityName { get; set; }
        public string StatePrefix { get; set; }
        public List<AuxFields> CustomFields { get; set; }
    }
}
