﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
   public class PlaiConfiguration
    {
        public LdapConfiguration LdapConfiguration { get; set; }

        public List<LdapAttribute> LdapUserAttribute { get; set; }

        public List<LdapAttribute> LdapGroupAttribute { get; set; }

        public List<LocationMapping> LocationMappings { get; set; }

        public List<DefaultAccessProfile> DefaultAccessProfiles { get; set; }
    }
}
