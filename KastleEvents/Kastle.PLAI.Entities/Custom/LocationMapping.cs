﻿namespace Kastle.PLAI.Entities
{
    public class LocationMapping
    {
        public string LdapOuGuid { get; set; }
        public string LdapOuName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
