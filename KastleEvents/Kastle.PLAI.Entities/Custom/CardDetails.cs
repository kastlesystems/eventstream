﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class CardDetails
    {
        [XmlIgnore]
        public int CardID { get; set; }

        [XmlIgnore]
        public int ReaderTechId { get; set; }

        [XmlIgnore]
        public bool IsPIV { get; set; }

        [XmlElement("ExternalNumber", Order = 0)]
        public string ExternalNumber { get; set; }


        [XmlElement("CredentialUID", Order = 1)]
        public string CredentialUID { get; set; }
    }
}
