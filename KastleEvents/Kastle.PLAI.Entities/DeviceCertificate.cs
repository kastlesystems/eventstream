﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.DeviceCertificate Class for device certificate
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class DeviceCertificate
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the application.
        /// </summary>
        [XmlElement("Application")]
        public string Application { get; set; }

        /// <summary>
        /// Gets or sets the certificate description.
        /// </summary>
        [XmlElement("CertificateDesc")]
        public CertificateDescription CertificateDescription { get; set; }
    }
}
