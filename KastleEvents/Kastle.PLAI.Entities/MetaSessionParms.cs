﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetaSessionParms Class for meta session parms
    /// </summary>
    public class MetaSessionParms
    {
        /// <summary>
        /// Gets or sets the meta xport parms.
        /// </summary>
        [XmlElement("metaXportParms", Form = XmlSchemaForm.Unqualified)]
        public MetaXportParms[] metaXportParms { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }
    }
}
