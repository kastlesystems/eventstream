﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// StateOfCredential
    /// </summary>
    public enum StateOfCredential
    {
        /// <summary>
        /// The inactive
        /// </summary>
        Inactive,
        /// <summary>
        /// The active
        /// </summary>
        Active,
        /// <summary>
        /// The expired
        /// </summary>
        Expired,
        /// <summary>
        /// The lost
        /// </summary>
        Lost,
        /// <summary>
        /// The stolen
        /// </summary>
        Stolen,
        /// <summary>
        /// The damaged
        /// </summary>
        Damaged,
        /// <summary>
        /// The destroyed
        /// </summary>
        Destroyed,
    }

    /// <summary>
    /// IdentifierType
    /// </summary>
    public enum IdentifierType
    {
        /// <summary>
        /// The card
        /// </summary>
        Card,
        /// <summary>
        /// The pin
        /// </summary>
        [XmlEnumAttribute("PIN")]
        Pin,
        /// <summary>
        /// The biometric
        /// </summary>
        Biometric,
        /// <summary>
        /// The key fob
        /// </summary>
        KeyFob,
    }

    /// <summary>
    /// PsiaSpecTag
    /// </summary>
    public enum PsiaSpecTag
    {
        /// <summary>
        /// The ipmd
        /// </summary>
        [XmlEnumAttribute("ipmd")]
        Ipmd,

        /// <summary>
        /// The racm
        /// </summary>
        [XmlEnumAttribute("racm")]
        Racm,

        /// <summary>
        /// The video analytics
        /// </summary>
        [XmlEnumAttribute("videoAnalytics")]
        VideoAnalytics,

        /// <summary>
        /// The cmem
        /// </summary>
        [XmlEnumAttribute("cmem")]
        Cmem,

        /// <summary>
        /// The area control
        /// </summary>
        [XmlEnumAttribute("areaCtl")]
        AreaCtl,

        /// <summary>
        /// The csec
        /// </summary>
        [XmlEnumAttribute("csec")]
        Csec,

        /// <summary>
        /// The other psia
        /// </summary>
        [XmlEnumAttribute("other-PSIA")]
        OtherPSIA,

        /// <summary>
        /// The other private
        /// </summary>
        [XmlEnumAttribute("other-private")]
        OtherPrivate,
    }

    /// <summary>
    /// PsiaSpecProfileLevel
    /// </summary>
    public enum PsiaSpecProfileLevel
    {
        /// <summary>
        /// The core
        /// </summary>
        [XmlEnumAttribute("core")]
        Core,

        /// <summary>
        /// The core plus
        /// </summary>
        [XmlEnumAttribute("core+")]
        CorePlus,

        /// <summary>
        /// The basic
        /// </summary>
        [XmlEnumAttribute("basic")]
        Basic,

        /// <summary>
        /// The basic plus
        /// </summary>
        [XmlEnumAttribute("basic+")]
        BasicPlus,

        /// <summary>
        /// The extnded
        /// </summary>
        [XmlEnumAttribute("extnded")]
        Extnded,

        /// <summary>
        /// The extended plus
        /// </summary>
        [XmlEnumAttribute("extended+")]
        ExtendedPlus,

        /// <summary>
        /// The full
        /// </summary>
        [XmlEnumAttribute("full")]
        Full,

        /// <summary>
        /// The full plus
        /// </summary>
        [XmlEnumAttribute("full+")]
        FullPlus,

        /// <summary>
        /// The advanced
        /// </summary>
        [XmlEnumAttribute("advanced")]
        Advanced,

        /// <summary>
        /// The advanced plus
        /// </summary>
        [XmlEnumAttribute("advanced+")]
        AdvancedPlus,
    }

    /// <summary>
    /// ResourceType
    /// </summary>
    public enum ResourceType
    {
        /// <summary>
        /// The service
        /// </summary>
        [XmlEnumAttribute("service")]
        Service,

        /// <summary>
        /// The resource
        /// </summary>
        [XmlEnumAttribute("resource")]
        Resource,
    }

    /// <summary>
    /// AccessDeniedReason
    /// </summary>
    public enum AccessDeniedReason
    {
        /// <summary>
        /// The other
        /// </summary>
        Other,
        /// <summary>
        /// The unknown credential
        /// </summary>
        UnknownCredential,
        /// <summary>
        /// The invalid credential
        /// </summary>
        InvalidCredential,
        /// <summary>
        /// The inactive credential
        /// </summary>
        InactiveCredential,
        /// <summary>
        /// The expired credential
        /// </summary>
        ExpiredCredential,
        /// <summary>
        /// The lost credential
        /// </summary>
        LostCredential,
        /// <summary>
        /// The stolen credential
        /// </summary>
        StolenCredential,
        /// <summary>
        /// The invalid credential format
        /// </summary>
        InvalidCredentialFormat,
        /// <summary>
        /// The invalid facility code
        /// </summary>
        InvalidFacilityCode,
        /// <summary>
        /// The invalid issue code
        /// </summary>
        InvalidIssueCode,
        /// <summary>
        /// The authentication timeout
        /// </summary>
        AuthenticationTimeout,
        /// <summary>
        /// The authentication failure
        /// </summary>
        AuthenticationFailure,
        /// <summary>
        /// The maximum retries reached
        /// </summary>
        MaxRetriesReached,
        /// <summary>
        /// The inactive credential holder
        /// </summary>
        InactiveCredentialHolder,
        /// <summary>
        /// The expired credential holder
        /// </summary>
        ExpiredCredentialHolder,
        /// <summary>
        /// The not permitted
        /// </summary>
        NotPermitted,
        /// <summary>
        /// The not permitted at this time
        /// </summary>
        NotPermittedAtThisTime,
        /// <summary>
        /// The anti passback
        /// </summary>
        AntiPassback,
        /// <summary>
        /// The access overridden
        /// </summary>
        AccessOverridden,
        /// <summary>
        /// The no asset
        /// </summary>
        NoAsset,
        /// <summary>
        /// The no escort
        /// </summary>
        NoEscort,
        /// <summary>
        /// The occupancy limit reached
        /// </summary>
        OccupancyLimitReached,
        /// <summary>
        /// The credential not presented
        /// </summary>
        CredentialNotPresented,
        /// <summary>
        /// The use limit reached
        /// </summary>
        UseLimitReached,
        /// <summary>
        /// The partition closed
        /// </summary>
        PartitionClosed,
        /// <summary>
        /// The unauthorized
        /// </summary>
        Unauthorized,
        /// <summary>
        /// The biometric mismatch
        /// </summary>
        BiometricMismatch,
        /// <summary>
        /// The invalid pin
        /// </summary>
        InvalidPIN,
        /// <summary>
        /// The access overridden1
        /// </summary>
        [XmlEnumAttribute("AccessOverridden")]
        AccessOverridden1,
    }

    /// <summary>
    /// AccessGrantedReason
    /// </summary>
    public enum AccessGrantedReason
    {
        /// <summary>
        /// The other
        /// </summary>
        Other,
        /// <summary>
        /// The ok
        /// </summary>
        OK,
        /// <summary>
        /// The duress
        /// </summary>
        Duress,
    }

    /// <summary>
    /// ParamModMode
    /// </summary>
    public enum ParamModMode
    {
        /// <summary>
        /// The readwrite
        /// </summary>
        [XmlEnumAttribute("read-write")]
        Readwrite,

        /// <summary>
        /// The readonly
        /// </summary>
        [XmlEnumAttribute("read-only")]
        Readonly,
    }

    /// <summary>
    /// MetaFormat
    /// </summary>
    public enum MetaFormat
    {
        /// <summary>
        /// The GMCH psia
        /// </summary>
        [XmlEnumAttribute("gmch-psia")]
        GmchPsia,

        /// <summary>
        /// The XML psia
        /// </summary>
        [XmlEnumAttribute("xml-psia")]
        XmlPsia,
    }

    /// <summary>
    /// SessionProtocolType
    /// </summary>
    public enum SessionProtocolType
    {
        /// <summary>
        /// The rest synchronize session target send
        /// </summary>
        RESTSyncSessionTargetSend,

        /// <summary>
        /// The rest asynchronous session back source send
        /// </summary>
        RESTAsyncSessionBackSourceSend,

        /// <summary>
        /// The restrtp stream source out UDP
        /// </summary>
        RESTRTPStreamSrcOutUDP,

        /// <summary>
        /// The restrtp stream source out TCP
        /// </summary>
        RESTRTPStreamSrcOutTCP,

        /// <summary>
        /// The RTSPRTP stream source out
        /// </summary>
        RTSPRTPStreamSrcOut,

        /// <summary>
        /// The RTSPRTP stream source out interleaved
        /// </summary>
        RTSPRTPStreamSrcOutInterleaved,

        /// <summary>
        /// The rest asynchronous session back for receive
        /// </summary>
        RESTAsyncSessionBackForReceive,

        /// <summary>
        /// The restrtp stream in UDP
        /// </summary>
        RESTRTPStreamInUDP,

        /// <summary>
        /// The restrtp stream in TCP
        /// </summary>
        RESTRTPStreamInTCP,

        /// <summary>
        /// The email notification
        /// </summary>
        EmailNotification,

        /// <summary>
        /// The rest session source out raw TCP
        /// </summary>
        RESTSessionSrcOutRawTCP,

        /// <summary>
        /// The rest session into source raw TCP
        /// </summary>
        RESTSessionIntoSrcRawTCP,

        /// <summary>
        /// The rest stream source out raw UDP
        /// </summary>
        RESTStreamSrcOutRawUDP,

        /// <summary>
        /// The restrtp asynchronous stream source out UDP
        /// </summary>
        RESTRTPAsyncStreamSrcOutUDP,

        /// <summary>
        /// The rest asynchronous session source out raw TCP
        /// </summary>
        RESTAsyncSessionSrcOutRawTCP,

        /// <summary>
        /// The rest asynchronous session source out raw UDP
        /// </summary>
        RESTAsyncSessionSrcOutRawUDP,
    }

    /// <summary>
    /// SessionFlowType
    /// </summary>
    public enum SessionFlowType
    {
        /// <summary>
        /// The data stream
        /// </summary>
        [XmlEnumAttribute("datastream")]
        DataStream,

        /// <summary>
        /// The transaction
        /// </summary>
        [XmlEnumAttribute("transaction")]
        Transaction,

        /// <summary>
        /// The stream or transaction
        /// </summary>
        [XmlEnumAttribute("streamOrTransaction")]
        StreamOrTransaction,

        /// <summary>
        /// The stateful
        /// </summary>
        [XmlEnumAttribute("stateful")]
        Stateful,

        /// <summary>
        /// The stream and or stateful
        /// </summary>
        [XmlEnumAttribute("streamAndOrStateful")]
        StreamAndOrStateful,
    }

    /// <summary>
    /// MetaSessionPersistence
    /// </summary>
    public enum MetaSessionPersistence
    {
        /// <summary>
        /// The dynamic
        /// </summary>
        [XmlEnumAttribute("dynamic")]
        Dynamic,

        /// <summary>
        /// The static
        /// </summary>
        [XmlEnumAttribute("static")]
        Static,
    }

    /// <summary>
    /// CredentialAssignmentState
    /// </summary>
    public enum CredentialAssignmentState
    {
        Assigned,
        Unassigned,
        Unknown
    }

    /// <summary>
    /// IdentifierInfoValueEncoding
    /// </summary>
    public enum IdentifierInfoValueEncoding
    {
        /// <summary>
        /// The string
        /// </summary>
        String,
        /// <summary>
        /// The decimal
        /// </summary>
        Decimal,
        /// <summary>
        /// The hexadecimal
        /// </summary>
        Hexadecimal,
        /// <summary>
        /// The base64
        /// </summary>
        Base64,
    }

    /// <summary>
    /// CardFormatDataFieldType
    /// </summary>
    public enum CardFormatDataFieldType
    {
        /// <summary>
        /// The facility code
        /// </summary>
        FacilityCode,
        /// <summary>
        /// The card number
        /// </summary>
        CardNum,
        /// <summary>
        /// The issue code
        /// </summary>
        IssueCode,
        /// <summary>
        /// The fixed
        /// </summary>
        Fixed,
    }

    /// <summary>
    /// TimeMode
    /// </summary>
    public enum TimeMode
    {
        /// <summary>
        /// The NTP
        /// </summary>
        NTP,
        /// <summary>
        /// The manual
        /// </summary>
        manual,
    }
    /// <summary>
    /// CustomStatusString
    /// </summary>
    public enum CustomStatusString
    {
        /// <summary>
        /// The role impacting cardholder
        /// </summary>
        RoleImpactingCardholder,
        IssuerSignatureInValid,
        MultipleApExistWithSameNameConflict,
        EnterpriseOrInstitutionDefaultApExistWithSameNameConflict
    }
    /// <summary>
    /// SqlErrorNumber
    /// </summary>
    public enum SqlErrorNumber:int
    {
        MultipleApExistWithSameNameConflict = 60090,
        EnterpriseOrInstitutionDefaultApExistWithSameNameConflict = 60104
    }

    public enum LdapConfigurationType
    {
        LdapConfiguration=0,
        LdapUserAttribute=1,
        LdapGroupAttribute=2,
    }

    public enum CardHolderAddedState : int
    {
        AddedNew = 0,
        AddedExisting = 1,
        AddedMigration=2
    }

    public enum RoleAddedState : int
    {
        AddedNew = 0,
        AddedExisting = 1,
        AddedMigration = 2
    }

    public enum CardholderSyncResponse
    {
        Conflicted,
        UnIdentifiedError,
        CardholderNCAState,
        BuildingMovementStatus
    }

    public enum RoleSyncResponse
    { 
        RoleStateResponse,
        RoleImpactResponse
    }

    public enum WiegandCardFormatParityFieldType
    {
        Even,
        Odd
    }
    public enum UserType : int
    {
        IdentityConnect = 2,
        VisitorConnect = 4,
        Both = 6,
        SendEmail = 8      
    }
    public enum PersonType
    {
        LeaseHolder,
        Occupant,
        Staff,
        Default
    }

}
