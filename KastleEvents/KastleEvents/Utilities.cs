﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Data;


namespace KastleEvents
{
    public class Utilities
    {

        public static string GetSha256Hash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            var strBuilder = new StringBuilder();
            foreach (byte x in hash)
            {
                strBuilder.Append(string.Format("{0:x2}", x));
            }
            return strBuilder.ToString();
        }
/*
        public static string GetIssuerSignatureFromHeader(HttpActionContext actionContext)
        {
            IEnumerable<string> values;
            string issuerSignature = string.Empty;
            if (actionContext.Request.Headers.TryGetValues("IssuerSignature", out values))
            {
                issuerSignature = values.First();
            }
            return issuerSignature;
        }
*/
        public static string SerializeObject<T>(T pObject)
        {
            var memoryStream = new MemoryStream();

            var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            try
            {
                var xs = new XmlSerializer(typeof(T));

                xs.Serialize(xmlTextWriter, pObject);

                var doc = new XmlDocument();
                memoryStream.Position = 0;
                doc.Load(memoryStream);
                memoryStream.Close();
                // strip out default namespaces "xmlns:xsi" and "xmlns:xsd"
                if (doc.DocumentElement != null)
                {
                    doc.DocumentElement.Attributes.RemoveAll();
                }

                var sw = new StringWriter();
                var xw = new XmlTextWriter(sw);
                doc.WriteTo(xw);

                return sw.ToString();
            }
            finally
            {
                memoryStream.Close();
                xmlTextWriter.Close();
            }
        }


        public static T DeserializeObject<T>(string xmlString)
        {
            if (String.IsNullOrWhiteSpace(xmlString))
            {
                return default(T);
            }
            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(memStream);
            }
        }

    }
}