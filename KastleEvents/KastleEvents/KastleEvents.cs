﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

using Kastle.PLAI.Entities;
using log4net;

namespace KastleEvents
{
    class KastleEvents
    {
        const int STREAM_TIMER_MS = 10000;      // 10 seconds
        const int PROFILE_TIMER_MS = 60000;     // 60 seconds

        // Set the PSIA URL.
        //protected static string UrlPSIA = "https://mykastle.com/PSIA";
        protected static string UrlPSIA = "https://sandbox2.kastle.com/PSIA";
        protected static string UrlStream = UrlPSIA + "/Metadata/stream";
        protected static string UrlDeviceOwnership = UrlPSIA + "/CSEC/deviceOwnership";
        protected static string UrlProfile = UrlPSIA + "/profile";

        // Replace with your OwnerGuid and credential.
        protected string OwnerGuid = "{51d14e8a-91df-447d-807b-26e0ff740e83}";
        protected string Username = "user1";
        protected string Password = "password";

        protected string IssuerSignature = String.Empty;

        // This line is added in AssemblyInfo.cs.
        // [assembly: log4net.Config.XmlConfigurator(Watch = true)]
        public static ILog Logger
        {
            get { return LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); }
        }

        public void Run()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                   SecurityProtocolType.Tls11 |
                                   SecurityProtocolType.Tls12;

            // Insert your code here to retrieve your IssuerSignature from your safe 
            // permanent storage.
            while (String.IsNullOrWhiteSpace(IssuerSignature))
            {
                GetDeviceOwnership();
                // Save your IssuerSignature in your safe permanent storage.
                // You don't need to call GetDeviceOwnnership() if your credential 
                // and /or OwnerGuid has not changed and you have already retrieved 
                // your IssuerSignature.
                if (String.IsNullOrWhiteSpace(IssuerSignature))
                {
                    // Sleep 10 seconds before retrying.
                    Thread.Sleep(10000);
                }
                else
                {
                    break;
                }
            }

            OnTimedGetProfile();
            System.Timers.Timer profileTimer = new System.Timers.Timer(PROFILE_TIMER_MS);
            profileTimer.Elapsed += (sender, e) => { OnTimedGetProfile(); };
            profileTimer.Enabled = true;

            OnTimedGetEvents();
            System.Timers.Timer streamTimer = new System.Timers.Timer(STREAM_TIMER_MS);
            streamTimer.Elapsed += (sender, e) => { OnTimedGetEvents(); };
            streamTimer.Enabled = true;

            while (true)
            {
                Thread.Sleep(10000);
            }
        }

        // This method retrieves the Issuer Signature from the server.
        protected void GetDeviceOwnership()
        {
            try
            {
                UriBuilder builder = new UriBuilder(UrlDeviceOwnership);
                builder.Query = "OwnerGUID=" + Uri.EscapeUriString(OwnerGuid);
                var byteArray = Encoding.ASCII.GetBytes(Username + ":" + Password);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(builder.Uri);
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                request.Accept = "application/xml";
                request.ContentType = "application/xml";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string xmlOwnership = reader.ReadToEnd();

                        //Console.WriteLine("DeviceOwnership: {0}", xmlOwnership);
                        var ownershipCookie = Utilities.DeserializeObject<CSECOwnershipCookie>(xmlOwnership);
                        IssuerSignature = ownershipCookie.IssuerSignature;
                        if (!String.IsNullOrWhiteSpace(IssuerSignature) && IssuerSignature.Length > 6) {
                            // We avoid printing the full IssuerSignature in the logs.
                            Logger.InfoFormat("Received IssuerSignature (first 6 chars): {0}...", IssuerSignature.Substring(0, 6));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("GetDeviceOwnership Exception: {0}", ex.ToString());
                Logger.ErrorFormat("GetDeviceOwnership Exception: {0}", ex.ToString());
            }
        }

        // This method sends a Profile GET request. 
        // This is an essential call which is used as a heartbeat monitor on the server side.
        protected void OnTimedGetProfile()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(IssuerSignature))
                {
                    return;
                }

                UriBuilder builder = new UriBuilder(UrlProfile);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(builder.Uri);
                request.Headers.Add("IssuerSignature", IssuerSignature);
                request.Accept = "application/xml";
                request.ContentType = "application/xml";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string xmlProfile = reader.ReadToEnd();
                        //Console.WriteLine("Profile: {0}", xmlProfile);
                        var psiaProfile = Utilities.DeserializeObject<PsiaProfile>(xmlProfile);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("OnTimedGetProfile Exception: {0}", ex.ToString());
                Logger.ErrorFormat("OnTimedGetProfile Exception: {0}", ex.ToString());
            }
        }

        // This method gets called periodically to receive the available Area Control Events.
        public void OnTimedGetEvents()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(IssuerSignature))
                {
                    return;
                }

                UriBuilder builder = new UriBuilder(UrlStream);
                var byteArray = Encoding.ASCII.GetBytes(Username + ":" + Password);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(builder.Uri);
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                request.Headers.Add("IssuerSignature", IssuerSignature);
                request.Accept = "application/xml";
                request.ContentType = "application/xml";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string xmlAcel = reader.ReadToEnd();
                        //Console.WriteLine("Received Area Control Event List: {0}", xmlAcel);                        
                        AreaControlEventList acel = Utilities.DeserializeObject<AreaControlEventList>(xmlAcel);
                        if (acel.AreaControlEvent.Count > 0)
                        {
                            Logger.InfoFormat("Received Area Control Event List: Batch Size: {0} -- {1}",
                                               acel.AreaControlEvent.Count,
                                               xmlAcel);
                            foreach (AreaControlEvent ace in acel.AreaControlEvent)
                            {
                                // Process each AreaControlEvent object received.     
                                // InsertIntoEventConnectTable(ace);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("OnTimedGetEvents Exception: {0}", ex.ToString());
                Logger.ErrorFormat("OnTimedGetEvents Exception: {0}", ex.ToString());
            }
        }

        /*
        <?xml version="1.0" encoding="UTF-8"?>
        <AreaControlEventList xmlns="urn:psialliance-org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="" xsi:schemaLocation="urn:psialliance-org ACWG.xsd">
            <AreaControlEvent>
               <MetadataHeader>
                  <MetaVersion>1.0</MetaVersion>
                  <MetaID>/psialliance.org/AreaControl.Portal/access.granted/30913</MetaID>
                  <MetaSourceID>{57cdc16a-da79-4fa9-8f4a-42a032c49123}</MetaSourceID>
                  <MetaSourceLocalID>30913</MetaSourceLocalID>
                  <MetaTime>2001-12-31T12:00:00</MetaTime>
                  <MetaPriority>0</MetaPriority>
               </MetadataHeader>
               <EventData>
                  <ValueState>
                     <Granted>OK</Granted>
                  </ValueState>
                  <PortalID>
                     <!--Local ID of the location as used in the header-->
                     <ID>30913</ID>
                     <!--GUID associated with the Local ID of this location-->
                     <GUID>{57cdc16a-da79-4fa9-8f4a-42a032c49123}</GUID>
                     <!-- Full name of reader. First 6 characters identify building -->
                     <Name>DC0557 Reader 115</Name>
                     <CustomExtension>
                        <CustomExtensionName>https://cache.kastle.com/psia/customportalid</CustomExtensionName>
                        <ReaderLocationDescription>P1 Elevator Lobby Door B</ReaderLocationDescription>
                     </CustomExtension>
                  </PortalID>
                  <CredentialHolderInfo version="">
                     <!--Local ID of the credential holder.-->
                     <ID>301321123</ID>
                     <Name>Longo, Jeffrey</Name>
                     <GivenName>Jeffrey</GivenName>
                     <Surname>Longo</Surname>
                     <State>Active</State>
                     <!-- If an email is in our system, we will provide it.-->
                     <AttributeList>
                        <Attribute>
                           <Name>Organization</Name>
                           <Value>XYZ Properties - DC0557</Value>
                        </Attribute>
                        <Attribute>
                           <Name>Email</Name>
                           <Value>jlongo@kastle.com</Value>
                        </Attribute>
                     </AttributeList>
                  </CredentialHolderInfo>
                  <CredentialInfo version="">
                     <!-- Local ID of the card used -->
                     <ID>303214532</ID>
                     <State>Active</State>
                     <IdentifierInfoList>
                        <IdentifierInfo>
                           <Type>Card</Type>
                           <!-- Value is the card number as you'd expect it to appear in a report.
                                What we call an "external number"-->
                           <Value>2223-15323</Value>
                           <ValueEncoding>String</ValueEncoding>
                        </IdentifierInfo>
                     </IdentifierInfoList>
                     <PermissionIDList>
                        <!-- Ignore this element-->
                     </PermissionIDList>
                  </CredentialInfo>
               </EventData>
            </AreaControlEvent>
        </AreaControlEventList>
        */

        /*
                CREATE TABLE EventConnect (
                    NCEventDate datetime,
                    NCLastName varchar(255),
                    NCFirstName varchar(255),
                    NCGivenName varchar(255),
                    NCCardNum varchar(64),
                    NCCardID int,
                    NCOrg varchar(255),
                    NCEmail varchar(255),
                    NCReaderDesig varchar(255),
                    NCReaderDesc varchar(255),
                    NCBldgNum varchar(10),
                    NCGranted bit NOT NULL DEFAULT 0 
                );		

                CREATE INDEX idx_NCEventDate
                ON zzEventConnect (NCEventDate);

                CREATE INDEX idx_NCBldgNum
                ON zzEventConnect (NCBldgNum);
        */
        /*
        protected void InsertIntoEventConnectTable(AreaControlEvent ace)
        {                        
            string nca_conn_string = "Server=***;Database=***;User Id=***;Password=***;Trusted_Connection=***;";
            string lastName = String.Empty, firstName = String.Empty, givenName = String.Empty, cardNum = String.Empty,
                   org = String.Empty, email = String.Empty, readerDesig = String.Empty, readerDesc = String.Empty, 
                   bldgNum = String.Empty;
            bool granted = false;
            uint cardID = 0;

            if (ace.EventData.ValueState.Item.GetType() == typeof(AccessGrantedReason))
            {
                granted = true;
            }

            CredentialHolderInfo ch;
            if (ace.EventData.CredentialHolderInfoList.GetType() == typeof(CredentialHolderInfoList))
            {
                CredentialHolderInfoList lst = (CredentialHolderInfoList)ace.EventData.CredentialHolderInfoList;
                List<CredentialHolderInfo> chList = lst.CredentialHolderInfo;
                ch = chList[0];
            }
            else
            {
                ch = (CredentialHolderInfo)ace.EventData.CredentialHolderInfoList;
            }

            lastName = ch.Surname;
            firstName = ch.GivenName;
            givenName = ch.Name;
            foreach (var attr in ch.CardHolderAttributes)
            {
                if (attr.AttributeName == "Organization")
                {
                    // Organization of the cardholder
                    org = attr.AttributeValue;
                }
                if (attr.AttributeName == "Email")
                {
                    email = attr.AttributeValue;
                }
            }

            CredentialInfo ci;
            if (ace.EventData.CredentialInfoList.GetType() == typeof(CredentialInfoList))
            {
                CredentialInfoList lst = (CredentialInfoList)ace.EventData.CredentialInfoList;
                List<CredentialInfo> ciList = lst.CredentialInfo;
                ci = ciList[0];
            }
            else
            {
                ci = (CredentialInfo)ace.EventData.CredentialInfoList;
            }

            cardID = ci.ID;
            List<IdentifierInfo> iil = ci.IdentifierInfoList;
            foreach (var ii in iil)
            {
                cardNum = ii.Value;
                break;
            }

            ReferenceID ri;
            if (ace.EventData.PortalIDList.GetType() == typeof(PortalIDList))
            {
                PortalIDList lst = (PortalIDList)ace.EventData.PortalIDList;
                List<ReferenceID> riList = lst.PortalID;
                ri = riList[0];
            }
            else
            {
                ri = (ReferenceID)ace.EventData.PortalIDList;
            }

            readerDesig = ri.Name;
            if (readerDesig.Length > 6)
            {
                // Building number format: <two letter region code><4 digit number>, such as DC1234
                // Reader designator format: <building number> Reader <number>, such as DC1234 Reader 5
                bldgNum = readerDesig.Substring(0, 6);
            }
            for (int i = 0; i < ri.CustomExtension.Length; i++)
            {
                if (ri.CustomExtension[i].Any[0].Name == "ReaderLocationDescription")
                {
                    readerDesc = ri.CustomExtension[i].Any[0].InnerText;
                }
            }

            try
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(" INSERT INTO EventConnect(NCEventDate, NCLastName, NCFirstName, NCGivenName, NCCardNum, NCCardID, NCOrg, NCEmail, NCReaderDesig, NCReaderDesc, NCBldgNum, NCGranted) ");
                builder.Append(" VALUES (@EventDate, @LastName, @FirstName, @GivenName, @CardNum, @CardID, @Org, @Email, @ReaderDesig, @ReaderDesc, @BldgNum, @Granted) ");

                using (SqlConnection conn = new SqlConnection(nca_conn_string))
                using (SqlCommand command = new SqlCommand(builder.ToString(), conn))
                {
                    conn.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add("@EventDate", SqlDbType.DateTime).Value = ace.MetadataHeader.MetaTime;
                    command.Parameters.Add("@LastName", SqlDbType.VarChar).Value = lastName;
                    command.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = firstName;
                    command.Parameters.Add("@GivenName", SqlDbType.VarChar).Value = givenName;
                    command.Parameters.Add("@CardNum", SqlDbType.VarChar).Value = cardNum;
                    command.Parameters.Add("@CardID", SqlDbType.Int).Value = cardID;
                    command.Parameters.Add("@Org", SqlDbType.VarChar).Value = org;
                    command.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
                    command.Parameters.Add("@ReaderDesig", SqlDbType.VarChar).Value = readerDesig;
                    command.Parameters.Add("@ReaderDesc", SqlDbType.VarChar).Value = readerDesc;
                    command.Parameters.Add("@BldgNum", SqlDbType.VarChar).Value = bldgNum;
                    command.Parameters.Add("@Granted", SqlDbType.Bit).Value = granted;

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("UpdateIssuerSignature Exception: {0}", ex.ToString());
            }
        }
        */

    }
}
