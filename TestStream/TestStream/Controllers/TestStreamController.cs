﻿using System.Collections.Generic;
using System.Web.Http;
using log4net;

using Kastle.PLAI.Entities;
using TestStream.Helpers;


// This project is used to test the streaming events posted from PSIA server.
// PSIA protocol requires this server to be set at the PSIA root. So, replace
// TestStream with PSIA in production.
namespace TestStream.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    //[RequireHttps]
    //[RoutePrefix("PSIA")]
    public class TestStreamController : ApiController
    {
        private ILog Logger = LogManager.GetLogger(typeof(TestStreamController));
        // Construct your IssuerSignature from your OwnerGuid, Username and Password.
        public static string IssuerSignature
        {
            get;
            set; 
        }

        [Route("CSEC/deviceOwnership")]
        [HttpGet]
        public CSECOwnershipCookie GetDeviceOwnership([FromUri]string OwnerGuid,
                                                      [FromUri]string ExpireTime = null,
                                                      [FromUri]string MaxAge = null)
        {
            Logger.InfoFormat("Received Owner GUID: {0}", OwnerGuid);

            // Store your IssuerSignature securely in a permanent storage as it will be needed in subsequent calls.
            string Username = "user1";
            string Password = "password1";

            IssuerSignature = Utilities.GetSha256Hash(OwnerGuid + ":" + Username + ":" + Password);
            var response = new CSECOwnershipCookie();
            response.IssuerSignature = IssuerSignature;

            return response;
        }


        [Route("Metadata/clientStream")]
        [HttpPost]
        public ResponseStatus PostClientStream([FromBody] AreaControlEventList acel)
        {
            string receivedIssuerSignature = Utilities.GetIssuerSignatureFromHeader(this.ActionContext);

            /*
            <?xml version="1.0" encoding="UTF-8"?>
            <AreaControlEventList xmlns="urn:psialliance-org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="" xsi:schemaLocation="urn:psialliance-org ACWG.xsd">
                <AreaControlEvent>
                   <MetadataHeader>
                      <MetaVersion>1.0</MetaVersion>
                      <MetaID>/psialliance.org/AreaControl.Portal/access.granted/30913</MetaID>
                      <MetaSourceID>{57cdc16a-da79-4fa9-8f4a-42a032c49123}</MetaSourceID>
                      <MetaSourceLocalID>30913</MetaSourceLocalID>
                      <MetaTime>2001-12-31T12:00:00</MetaTime>
                      <MetaPriority>0</MetaPriority>
                   </MetadataHeader>
                   <EventData>
                      <ValueState>
                         <Granted>OK</Granted>
                      </ValueState>
                      <PortalID>
                         <!--Local ID of the location as used in the header-->
                         <ID>30913</ID>
                         <!--GUID associated with the Local ID of this location-->
                         <GUID>{57cdc16a-da79-4fa9-8f4a-42a032c49123}</GUID>
                         <!-- Full name of reader. First 6 characters identify building -->
                         <Name>DC0557 Reader 115</Name>
                         <CustomExtension>
                            <CustomExtensionName>https://cache.kastle.com/psia/customportalid</CustomExtensionName>
                            <ReaderLocationDescription>P1 Elevator Lobby Door B</ReaderLocationDescription>
                         </CustomExtension>
                      </PortalID>
                      <CredentialHolderInfo version="">
                         <!--Local ID of the credential holder.-->
                         <ID>301321123</ID>
                         <Name>Longo, Jeffrey</Name>
                         <GivenName>Jeffrey</GivenName>
                         <Surname>Longo</Surname>
                         <State>Active</State>
                         <!-- If an email is in our system, we will provide it.-->
                         <AttributeList>
                            <Attribute>
                               <Name>Organization</Name>
                               <Value>XYZ Properties - DC0557</Value>
                            </Attribute>
                            <Attribute>
                               <Name>Email</Name>
                               <Value>jlongo@kastle.com</Value>
                            </Attribute>
                         </AttributeList>
                      </CredentialHolderInfo>
                      <CredentialInfo version="">
                         <!-- Local ID of the card used -->
                         <ID>303214532</ID>
                         <State>Active</State>
                         <IdentifierInfoList>
                            <IdentifierInfo>
                               <Type>Card</Type>
                               <!-- Value is the card number as you'd expect it to appear in a report.
                                    What we call an "external number"-->
                               <Value>2223-15323</Value>
                               <ValueEncoding>String</ValueEncoding>
                            </IdentifierInfo>
                         </IdentifierInfoList>
                         <PermissionIDList>
                            <!-- Ignore this element-->
                         </PermissionIDList>
                      </CredentialInfo>
                   </EventData>
                </AreaControlEvent>
            </AreaControlEventList>
            */

            foreach (AreaControlEvent ace in acel.AreaControlEvent)
            {
                if (ace.EventData != null)
                {
                    // This is not a marker event.
                    string strAce = Utilities.SerializeObject<AreaControlEvent>(ace);
                    Logger.InfoFormat("Received event: {0}", strAce);
                    // Utilities.InsertIntoEventConnectTable(ace);
                }
            }

            var response = new ResponseStatus();
            // Your IssuerSignature is constructed by using your OwnerGuid, Username and Password.
            if (IssuerSignature == receivedIssuerSignature)
            {
                response.StatusCode = 1;
                response.StatusString = "OK";
            }
            else
            {
                response.StatusCode = 3;
                response.StatusString = "DeviceError";
            }
            
            return response;
        }


        [Route("Profile")]
        [HttpGet]
        public PsiaProfile GetProfile()
        {
            /*
            <?xml version="1.0" encoding="UTF-8"?>
            <PsiaProfile xmlns="urn:psiallianceorg" version="1.1">
               <systemID>{661da097-ba20-4416-a465-b33318179235}</systemID>
               <nativeID>{661da097-ba20-4416-a465-b33318179235}</nativeID>
               <psiaServiceVersion>3</psiaServiceVersion>
               <primaryPsiaSpec>
                  <psiaSpecName>areaCtl</psiaSpecName>
                  <psiaSpecVersion>3</psiaSpecVersion>
                  <psiaSpecProfile>core</psiaSpecProfile>
               </primaryPsiaSpec>
               <profileList>
                  <psiaProfileDefn>
                     <psiaProfileName>PLAIBase</psiaProfileName>
                     <psiaProfileVersion>1</psiaProfileVersion>
                     <psiaSpec>areaCtl</psiaSpec>
                  </psiaProfileDefn>
                  <psiaProfileDefn>
                     <psiaProfileName>PLAIFunctionalRolesOption</psiaProfileName>
                     <psiaProfileVersion>1</psiaProfileVersion>
                     <psiaSpec>areaCtl</psiaSpec>
                  </psiaProfileDefn>
                  <psiaProfileDefn>
                     <psiaProfileName>PLAIRawLocationOption</psiaProfileName>
                     <psiaProfileVersion>1</psiaProfileVersion>
                     <psiaSpec>areaCtl</psiaSpec>
                  </psiaProfileDefn>
               </profileList>
            </PsiaProfile>
            */

            // Replace with your own GUIDs.
            var response = new PsiaProfile
            {
                SystemID = "{661da097-ba20-4416-a465-b33318179235}",
                NativeID = "{661da097-ba20-4416-a465-b33318179235}",
                PsiaServiceVersion = 3,
                PrimaryPsiaSpec = new PsiaSpecDecl()
                {
                    PsiaSpecName = PsiaSpecTag.AreaCtl,
                    PsiaSpecVersion = 3,
                    PsiaSpecProfile = PsiaSpecProfileLevel.Core
                },
                ProfileList = new List<PsiaProfileDefn>
                {
                    new PsiaProfileDefn
                    {
                        PsiaProfileName = "PLAIBase",
                        PsiaProfileVersion = 1,
                        PsiaSpec = PsiaSpecTag.AreaCtl
                    },
                    new PsiaProfileDefn
                    {
                        PsiaProfileName = "PLAIFunctionalRolesOption",
                        PsiaProfileVersion = 1,
                        PsiaSpec = PsiaSpecTag.AreaCtl
                    },
                    new PsiaProfileDefn
                    {
                        PsiaProfileName = "PLAIRawLocationOption",
                        PsiaProfileVersion = 1,
                        PsiaSpec = PsiaSpecTag.AreaCtl
                    }
                }
            };

            return response;
        }

        [Route("CSEC/deviceOwnership")]
        [HttpDelete]
        public ResponseStatus DeleteDeviceOwnership([FromUri]string ResetOwnership)
        {
            string receivedIssuerSignature = Utilities.GetIssuerSignatureFromHeader(this.ActionContext);
            var response = new ResponseStatus();
            if (IssuerSignature == receivedIssuerSignature)
            {
                response.StatusCode = 1;
                response.StatusString = "OK";
            }
            else
            {
                response.StatusCode = 3;
                response.StatusString = "DeviceError";
            }

            return response;
        }


    }
}