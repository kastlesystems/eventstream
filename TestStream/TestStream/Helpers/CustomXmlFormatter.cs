﻿using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;

namespace TestStream.Helpers
{
    public class CustomXmlFormatter : BufferedMediaTypeFormatter
    {
        public CustomXmlFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xml"));
        }

        public override bool CanReadType(Type type)
        {
            return true;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override void WriteToStream(Type type, object value, Stream writeStream, HttpContent content)
        {
            Encoding utf8EncodingWithNoByteOrderMark = new UTF8Encoding(false);
            using (XmlWriter writer = new XmlTextWriter(writeStream, utf8EncodingWithNoByteOrderMark))
            {
                var namespaces = new XmlSerializerNamespaces();
                {
                    namespaces.Add(string.Empty, "urn:psialliance-org");
                    var serializer = new XmlSerializer(type, "urn:psialliance-org");
                    serializer.Serialize(writer, value, namespaces);
                }
            }
        }

        public override object ReadFromStream(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var serializer = new XmlSerializer(type);
            return serializer.Deserialize(readStream);
        }

    }
}
