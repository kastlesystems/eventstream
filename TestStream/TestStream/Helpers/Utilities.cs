﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Xml;
using System.Xml.Serialization;
using System.Data;


namespace TestStream.Helpers
{
    public class Utilities
    {

        public static string GetSha256Hash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            var strBuilder = new StringBuilder();
            foreach (byte x in hash)
            {
                strBuilder.Append(string.Format("{0:x2}", x));
            }
            return strBuilder.ToString();
        }

        public static string GetIssuerSignatureFromHeader(HttpActionContext actionContext)
        {
            IEnumerable<string> values;
            string issuerSignature = string.Empty;
            if (actionContext.Request.Headers.TryGetValues("IssuerSignature", out values))
            {
                issuerSignature = values.First();
            }
            return issuerSignature;
        }

        public static string SerializeObject<T>(T pObject)
        {
            var memoryStream = new MemoryStream();

            var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            try
            {
                var xs = new XmlSerializer(typeof(T));

                xs.Serialize(xmlTextWriter, pObject);

                var doc = new XmlDocument();
                memoryStream.Position = 0;
                doc.Load(memoryStream);
                memoryStream.Close();
                // strip out default namespaces "xmlns:xsi" and "xmlns:xsd"
                if (doc.DocumentElement != null)
                {
                    doc.DocumentElement.Attributes.RemoveAll();
                }

                var sw = new StringWriter();
                var xw = new XmlTextWriter(sw);
                doc.WriteTo(xw);

                return sw.ToString();
            }
            finally
            {
                memoryStream.Close();
                xmlTextWriter.Close();
            }
        }

        public static T DeserializeObject<T>(string xmlString)
        {
            if (String.IsNullOrWhiteSpace(xmlString))
            {
                return default(T);
            }
            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(memStream);
            }
        }


        /*
                CREATE TABLE EventConnect (
                    NCEventDate datetime,
                    NCLastName varchar(255),
                    NCFirstName varchar(255),
                    NCGivenName varchar(255),
                    NCCardNum varchar(64),
                    NCCardID int,
                    NCOrg varchar(255),
                    NCEmail varchar(255),
                    NCReaderDesig varchar(255),
                    NCReaderDesc varchar(255),
                    NCBldgNum varchar(10),
                    NCGranted bit NOT NULL DEFAULT 0 
                );		

                CREATE INDEX idx_NCEventDate
                ON zzEventConnect (NCEventDate);

                CREATE INDEX idx_NCBldgNum
                ON zzEventConnect (NCBldgNum);
        */
        /*
        public static void InsertIntoEventConnectTable(AreaControlEvent ace)
        {                        
            string nca_conn_string = "Server=***;Database=***;User Id=***;Password=***;Trusted_Connection=***;";
            string lastName = String.Empty, firstName = String.Empty, givenName = String.Empty, cardNum = String.Empty,
                   org = String.Empty, email = String.Empty, readerDesig = String.Empty, readerDesc = String.Empty, 
                   bldgNum = String.Empty;
            bool granted = false;
            uint cardID = 0;

            if (ace.EventData.ValueState.Item.GetType() == typeof(AccessGrantedReason))
            {
                granted = true;
            }

            CredentialHolderInfo ch;
            if (ace.EventData.CredentialHolderInfoList.GetType() == typeof(CredentialHolderInfoList))
            {
                CredentialHolderInfoList lst = (CredentialHolderInfoList)ace.EventData.CredentialHolderInfoList;
                List<CredentialHolderInfo> chList = lst.CredentialHolderInfo;
                ch = chList[0];
            }
            else
            {
                ch = (CredentialHolderInfo)ace.EventData.CredentialHolderInfoList;
            }

            lastName = ch.Surname;
            firstName = ch.GivenName;
            givenName = ch.Name;
            foreach (var attr in ch.CardHolderAttributes)
            {
                if (attr.AttributeName == "Organization")
                {
                    // Organization of the cardholder
                    org = attr.AttributeValue;
                }
                if (attr.AttributeName == "Email")
                {
                    email = attr.AttributeValue;
                }
            }

            CredentialInfo ci;
            if (ace.EventData.CredentialInfoList.GetType() == typeof(CredentialInfoList))
            {
                CredentialInfoList lst = (CredentialInfoList)ace.EventData.CredentialInfoList;
                List<CredentialInfo> ciList = lst.CredentialInfo;
                ci = ciList[0];
            }
            else
            {
                ci = (CredentialInfo)ace.EventData.CredentialInfoList;
            }

            cardID = ci.ID;
            List<IdentifierInfo> iil = ci.IdentifierInfoList;
            foreach (var ii in iil)
            {
                cardNum = ii.Value;
                break;
            }

            ReferenceID ri;
            if (ace.EventData.PortalIDList.GetType() == typeof(PortalIDList))
            {
                PortalIDList lst = (PortalIDList)ace.EventData.PortalIDList;
                List<ReferenceID> riList = lst.PortalID;
                ri = riList[0];
            }
            else
            {
                ri = (ReferenceID)ace.EventData.PortalIDList;
            }

            readerDesig = ri.Name;
            if (readerDesig.Length > 6)
            {
                // Building number format: <two letter region code><4 digit number>, such as DC1234
                // Reader designator format: <building number> Reader <number>, such as DC1234 Reader 5
                bldgNum = readerDesig.Substring(0, 6);
            }
            for (int i = 0; i < ri.CustomExtension.Length; i++)
            {
                if (ri.CustomExtension[i].Any[0].Name == "ReaderLocationDescription")
                {
                    readerDesc = ri.CustomExtension[i].Any[0].InnerText;
                }
            }

            try
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(" INSERT INTO EventConnect(NCEventDate, NCLastName, NCFirstName, NCGivenName, NCCardNum, NCCardID, NCOrg, NCEmail, NCReaderDesig, NCReaderDesc, NCBldgNum, NCGranted) ");
                builder.Append(" VALUES (@EventDate, @LastName, @FirstName, @GivenName, @CardNum, @CardID, @Org, @Email, @ReaderDesig, @ReaderDesc, @BldgNum, @Granted) ");

                using (SqlConnection conn = new SqlConnection(nca_conn_string))
                using (SqlCommand command = new SqlCommand(builder.ToString(), conn))
                {
                    conn.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add("@EventDate", SqlDbType.DateTime).Value = ace.MetadataHeader.MetaTime;
                    command.Parameters.Add("@LastName", SqlDbType.VarChar).Value = lastName;
                    command.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = firstName;
                    command.Parameters.Add("@GivenName", SqlDbType.VarChar).Value = givenName;
                    command.Parameters.Add("@CardNum", SqlDbType.VarChar).Value = cardNum;
                    command.Parameters.Add("@CardID", SqlDbType.Int).Value = cardID;
                    command.Parameters.Add("@Org", SqlDbType.VarChar).Value = org;
                    command.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
                    command.Parameters.Add("@ReaderDesig", SqlDbType.VarChar).Value = readerDesig;
                    command.Parameters.Add("@ReaderDesc", SqlDbType.VarChar).Value = readerDesc;
                    command.Parameters.Add("@BldgNum", SqlDbType.VarChar).Value = bldgNum;
                    command.Parameters.Add("@Granted", SqlDbType.Bit).Value = granted;

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("UpdateIssuerSignature Exception: {0}", ex.ToString());
            }
        }
        */

    }
}