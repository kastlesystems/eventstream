﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.ResourceList Class for resource list
    /// </summary>
    [XmlRoot("ResourceList", Namespace = "urn:psialliance-org")]
    public class ResourceList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the resources.
        /// </summary>
        [XmlElement("Resource")]
        public List<Resource> Resources { get; set; }
    }
}
