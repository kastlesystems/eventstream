﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PartitionIDList Class for partition identifier list
    /// </summary>
    public class PartitionIDList
    {
        /// <summary>
        /// Gets or sets the partition identifier.
        /// </summary>
        [XmlElement("PartitionID")]
        public List<ReferenceID> PartitionID { get; set; }
    }
}
