﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.EventData Class for event data
    /// </summary>
    public class EventData
    {
        /// <summary>
        /// Gets or sets the state of the value.
        /// </summary>
        /// <value>
        /// The state of the value.
        /// </value>
        public EventValueState ValueState { get; set; }

        /// <summary>
        /// Gets or sets the input identifier list.
        /// </summary>
        [XmlElement("InputID", typeof(ReferenceID)), XmlElement("InputIDList", typeof(InputIDList))]
        public object InputIDList { get; set; }

        /// <summary>
        /// Gets or sets the output identifier list.
        /// </summary>
        [XmlElement("OutputID", typeof(ReferenceID)), XmlElement("OutputIDList", typeof(OutputIDList))]
        public object OutputIDList { get; set; }

        /// <summary>
        /// Gets or sets the partition identifier list.
        /// </summary>
        [XmlElement("PartitionID", typeof(ReferenceID)), XmlElement("PartitionIDList", typeof(PartitionIDList))]
        public object PartitionIDList { get; set; }

        /// <summary>
        /// Gets or sets the portal identifier list.
        /// </summary>
        [XmlElement("PortalID", typeof(ReferenceID)), XmlElement("PortalIDList", typeof(PortalIDList))]
        public object PortalIDList { get; set; }

        /// <summary>
        /// Gets or sets the zone identifier list.
        /// </summary>
        [XmlElement("ZoneID", typeof(ReferenceID)), XmlElement("ZoneIDList", typeof(ZoneIDList))]
        public object ZoneIDList { get; set; }

        /// <summary>
        /// Gets or sets the holiday identifier list.
        /// </summary>
        [XmlElement("HolidayID", typeof(ReferenceID)), XmlElement("HolidayIDList", typeof(HolidayIDList))]
        public object HolidayIDList { get; set; }

        /// <summary>
        /// Gets or sets the time schedule identifier list.
        /// </summary>
        [XmlElement("TimeScheduleID", typeof(ReferenceID)), XmlElement("TimeScheduleIDList", typeof(TimeScheduleIDList))]
        public object TimeScheduleIDList { get; set; }

        /// <summary>
        /// Gets or sets the permission identifier list.
        /// </summary>
        [XmlElement("PermissionID", typeof(ReferenceID)), XmlElement("PermissionIDList", typeof(PermissionIDList))]
        public object PermissionIDList { get; set; }

        /// <summary>
        /// Gets or sets the credential number list.
        /// </summary>
        [XmlElement("CredentialNumber", typeof(StringReference)), XmlElement("CredentialNumberList", typeof(CredentialNumberList))]
        public object CredentialNumberList { get; set; }

        /// <summary>
        /// Gets or sets the credential identifier list.
        /// </summary>
        [XmlElement("CredentialID", typeof(ReferenceID)), XmlElement("CredentialIDList", typeof(CredentialIDList))]
        public object CredentialIDList { get; set; }

        /// <summary>
        /// Gets or sets the credential holder identifier list.
        /// </summary>
        [XmlElement("CredentialHolderID", typeof(ReferenceID)), XmlElement("CredentialHolderIDList", typeof(CredentialHolderIDList))]
        public object CredentialHolderIDList { get; set; }

        /// <summary>
        /// Gets or sets the credential information list.
        /// </summary>
        [XmlElement("CredentialInfoList", typeof(CredentialInfoList)), XmlElement("CredentialInfo", typeof(CredentialInfo))]
        public object CredentialInfoList { get; set; }

        /// <summary>
        /// Gets or sets the credential holder information list.
        /// </summary>
        [XmlElement("CredentialHolderInfoList", typeof(CredentialHolderInfoList)), XmlElement("CredentialHolderInfo", typeof(CredentialHolderInfo))]
        public object CredentialHolderInfoList { get; set; }


        [XmlElement("CardFormatInfoList", typeof(CardFormatInfoList))]
        public object CardFormatInfo { get; set; }

        /// <summary>
        /// Gets or sets the information.
        /// </summary>
        public string Info { get; set; }

        /// <summary>
        /// Gets or sets the event data extension.
        /// </summary>
        [XmlElement("EventDataExtension")]
        public AreaControlExtension[] EventDataExtension { get; set; }
    }
}
