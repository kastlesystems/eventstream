﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderImageList Class for credential holder image list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CredentialHolderImageList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the credential holder image.
        /// </summary>
        [XmlElement("CredentialHolderImage")]
        public List<CredentialHolderImage> CredentialHolderImage { get; set; }
    }
}
