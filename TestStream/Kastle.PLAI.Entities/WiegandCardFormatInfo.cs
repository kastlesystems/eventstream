﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.WeigandCardFormatInfo Class for Weigand Card Format information
    /// </summary>
    [XmlRoot("WiegandCardFormatInfo", Namespace = "urn:psialliance-org")]
    public class WiegandCardFormatInfo
    {
        [XmlAttribute("version")]
        public string Version { get; set; }

        [XmlElement("ID", Order = 0)]
        public uint Id { get; set; }

        [XmlElement("UID", Order = 1)]
        public string UID { get; set; }

        [XmlElement("URI", Order = 2)]
        public string URI { get; set; }

        [XmlElement("Name", Order = 3)]
        public string Name { get; set; }

        [XmlElement("Description", Order = 4)]
        public string Description { get; set; }

        [XmlElement("Length", Order = 5)]
        public int Length { get; set; }

        [XmlElement("Proprietary", typeof(bool), Order = 6)]
        [XmlElement("DataFieldList", typeof(WiegandCardFormatDataFieldList), Order = 6)]
        public object Proprietary { get; set; }

        [XmlElement("CardFormatId", Order = 7)]
        public int CardFormatId { get; set; }
    }
}
