﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderInfo Class for credential holder information
    /// </summary>
    [XmlRoot("CredentialHolderInfo", Namespace = "urn:psialliance-org")]
    public class CredentialHolderInfo
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("ID", Order = 0)]
        public uint Id { get; set; }

        /// <summary>
        /// Gets or sets the u identifier.
        /// </summary>
        [XmlElement("UID", Order = 1)]
        public string UId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("Name", Order = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the given.
        /// </summary>
        /// <value>
        /// The name of the given.
        /// </value>
        [XmlElement("GivenName", Order = 3)]
        public string GivenName { get; set; }

        /// <summary>
        /// Gets or sets the name of the middle.
        /// </summary>
        /// <value>
        /// The name of the middle.
        /// </value>
        [XmlElement("MiddleName", Order = 4)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        [XmlElement("Surname", Order = 5)]
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlElement("Description", Order = 6)]
        public string Description { get; set; }


        /// <summary>
        /// Gets or sets the cardholder's activity starting date.
        /// </summary>
        [XmlElement("ActiveFrom", Order = 7)]
        public DateTime ActiveFrom { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active from specified].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [active from specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ActiveFromSpecified { get; set; }

        /// <summary>
        /// Gets or sets the cardholder's activity ending date.
        /// </summary>
        [XmlElement("ActiveTill", Order = 8)]
        public DateTime ActiveTill { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active till specified].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [active till specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ActiveTillSpecified { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        [XmlElement("State", Order = 9)]
        public StateOfCredential State { get; set; }

        /// <summary>
        /// Gets or sets the role u identifier list.
        /// </summary>
        [XmlArray("RoleIDList", Order = 10)]
        [XmlArrayItem("RoleID")]
        public List<RoleUIDs> RoleUIdList { get; set; }

        /// <summary>
        /// Gets or sets the card holder attributes.
        /// </summary>
        [XmlArray("AttributeList", Order = 11)]
        [XmlArrayItem("Attribute")]
        public List<CardHolderAttribute> CardHolderAttributes { get; set; }

        /// <summary>
        /// Gets or sets the event data extension.
        /// </summary>
        [XmlElement("CredentialHolderInfoExtension", Order = 12)]
        public AreaControlExtension CredentialHolderInfoExtension { get; set; }

        [XmlAnyAttribute]
        public XmlAttribute[] AnyAttributes { get; set; }

    }

    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CardHolderAttribute Class for card holder attribute
    /// </summary>
    public class CardHolderAttribute
    {
        /// <summary>
        /// Gets or sets the name of the attribute.
        /// </summary>
        /// <value>
        /// The name of the attribute.
        /// </value>
        [XmlElement("Name", Order = 0)]
        public string AttributeName { get; set; }

        /// <summary>
        /// Gets or sets the attribute value.
        /// </summary>
        [XmlElement("Value", Order = 1)]
        public string AttributeValue { get; set; }
    }

    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.RoleUIDs Class for role UI ds
    /// </summary>
    public class RoleUIDs
    {
        [XmlAnyAttribute]
        public XmlAttribute[] XAttributes { get; set; }

        [XmlElement("GUID", Order = 0)]
        public string RoleUId { get; set; }

        [XmlElement("Name", Order = 1)]
        public string Name { get; set; }

    }

}
