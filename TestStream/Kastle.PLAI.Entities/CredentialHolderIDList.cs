﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderIDList Class for credential holder identifier list
    /// </summary>
    public class CredentialHolderIDList
    {
        /// <summary>
        /// Gets or sets the credential holder identifier.
        /// </summary>
        [XmlElement("CredentialHolderID")]
        public List<ReferenceID> CredentialHolderID { get; set; }
    }
}
