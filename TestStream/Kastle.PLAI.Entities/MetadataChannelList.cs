﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetadataChannelList Class for metadata channel list
    /// </summary>
    public class MetadataChannelList
    {
        /// <summary>
        /// Gets or sets the meta channel.
        /// </summary>
        [XmlElement("metaChannel", Form = XmlSchemaForm.Unqualified)]
        public uint[] MetaChannel { get; set; }
    }
}
