﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CSECOwnershipStatus Class for csec ownership status
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CSECOwnershipStatus
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CSECOwnershipStatus"/> is owned.
        /// </summary>
        /// <value>
        ///   <c>true</c> if owned; otherwise, <c>false</c>.
        /// </value>
        [XmlElement("owned", Order = 0)]
        public bool Owned { get; set; }

        /// <summary>
        /// Gets or sets the owner unique identifier.
        /// </summary>
        [XmlElement("ownerGUID", Order = 1)]
        public string OwnerGUID { get; set; }
    }
}
