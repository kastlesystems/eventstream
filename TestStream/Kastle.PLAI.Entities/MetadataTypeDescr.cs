﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetadataTypeDescr Class for metadata type description
    /// </summary>
    public class MetadataTypeDescr
    {
        /// <summary>
        /// Gets or sets the type of the metdata.
        /// </summary>
        /// <value>
        /// The type of the metdata.
        /// </value>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string metdataType { get; set; }

        /// <summary>
        /// Gets or sets the metadata priority.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public byte metadataPriority { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [metadata priority specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [metadata priority specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metadataPrioritySpecified { get; set; }

        /// <summary>
        /// Gets or sets the metadata item mode.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public ParamModMode metadataItemMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [metadata item mode specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [metadata item mode specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metadataItemModeSpecified { get; set; }
    }
}
