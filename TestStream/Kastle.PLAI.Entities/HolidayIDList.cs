﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.HolidayIDList Class for holiday identifier list
    /// </summary>
    public class HolidayIDList
    {
        /// <summary>
        /// Gets or sets the holiday identifier.
        /// </summary>
        [XmlElement("HolidayID")]
        public List<ReferenceID> HolidayID { get; set; }
    }
}
