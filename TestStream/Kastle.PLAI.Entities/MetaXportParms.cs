﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetaXportParms Class for meta xport parms
    /// </summary>
    public class MetaXportParms
    {
        /// <summary>
        /// Gets or sets the meta session identifier.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public uint metaSessionID { get; set; }

        /// <summary>
        /// Gets or sets the meta format.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public MetaFormat metaFormat { get; set; }

        /// <summary>
        /// Gets or sets the type of the meta session.
        /// </summary>
        /// <value>
        /// The type of the meta session.
        /// </value>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public SessionProtocolType metaSessionType { get; set; }

        /// <summary>
        /// Gets or sets the type of the meta session flow.
        /// </summary>
        /// <value>
        /// The type of the meta session flow.
        /// </value>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public SessionFlowType metaSessionFlowType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [meta session role specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meta session role specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metaSessionRoleSpecified { get; set; }


        /// <summary>
        /// Gets or sets the item.
        /// </summary>
        [XmlElement("targetHostName", typeof(string), Form = XmlSchemaForm.Unqualified), XmlElement("targetIPAddress", typeof(string), Form = XmlSchemaForm.Unqualified), 
            XmlElement("targetIPv6Address", typeof(string), Form = XmlSchemaForm.Unqualified), XmlChoiceIdentifierAttribute("ItemElementName")]
        public string Item { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [netcast mode specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [netcast mode specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool netcastModeSpecified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [transaction ack].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [transaction ack]; otherwise, <c>false</c>.
        /// </value>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public bool transactionAck { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [transaction ack specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [transaction ack specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool transactionAckSpecified { get; set; }


        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        [XmlElement("metadataChannelList", typeof(MetadataChannelList), Form = XmlSchemaForm.Unqualified), XmlElement("metadataNameList", typeof(MetadataNameList), Form = XmlSchemaForm.Unqualified),
            XmlElement("metadataXChannelList", typeof(MetadataChannelList), Form = XmlSchemaForm.Unqualified), XmlChoiceIdentifierAttribute("ItemsElementName")]
        public object[] Items { get; set; }

        /// <summary>
        /// Gets or sets the meta session persistence.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public MetaSessionPersistence metaSessionPersistence { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [meta session persistence specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meta session persistence specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metaSessionPersistenceSpecified { get; set; }
    }
}
