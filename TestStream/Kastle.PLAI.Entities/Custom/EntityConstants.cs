﻿namespace Kastle.PLAI.Entities.Custom
{
    /// <summary>
    /// Entity Constants to be used as additional card holder attributes
    /// </summary>
    public static class EntityConstants
    {
        public const string Email = "Email";
        public const string FirstName = "First Name";
        public const string LastName = "Last Name";
        public const string LDAPUniqueId = "LDAP Unique Id";
        public const string FirstNameValue = "/psialliance.org/AreaControl.CredentialHolderAttribute/Givenname";
        public const string LastNameValue = "/psialliance.org/AreaControl.CredentialHolderAttribute/Surname";
        public const string LDAPUniqueIdValue = "/psialliance.org/AreaControl.CredentialHolderAttribute/Uid";
        public const string EmailValue = "/psialliance.org/AreaControl.CredentialHolderAttribute/Email";
        public const string RegionValue = "/kastle.com/AreaControl.CredentialHolderAttribute/AccessProfile/Region";
        public const string DisableAttribute = "/psialliance.org/AreaControl.CredentialHolderAttribute/DisableAttribute";
        public const string DisableAttributeValue = "Disable Attribute";

        public const string UniqueGuid = "/psialliance.org/AreaControl.RoleAttribute/Uid";
        public const string RoleName = "/psialliance.org/AreaControl.RoleAttribute/RoleName";
        public const string MappedWithOUORCN = "/kastle.com/AreaControl.RoleAttribute/MappingOU";

        public const string UniqueGuidValue = "Ldap Unique Id";
        public const string RoleNameValue = "Group Name";
        public const string DirectoryConnector = "Directory Connector";

        
        public const string Name = "Name";
        public const string MiddleName = "MiddleName";
        public const string EmployeeId = "EmployeeId";
        public const string Phone = "Phone";
        public const string Mobile = "Mobile";
        public const string Fax = "Fax";
        public const string Department = "Department";
        public const string Title = "Title";
        public const string SSN = "SSN";
        public const string Vehicle = "Vehicle";
        public const string EndDate = "EndDate";
        public const string Company = "Company";
        public const string StreetAddress = "StreetAddress";
        public const string PostalCode = "PostalCode";
        public const string Country = "Country";
        public const string City = "City";
        public const string State = "State";
        public const string Disability = "Disability";
        public const string Citizenship = "Citizenship";
        public const string SecurityLevel = "SecurityLevel";
        public const string Certification = "Certification";
        public const string IsContractor = "IsContractor";
        public const string ActivationDate = "ActivationDate";

        public const string CredentialHolderCountHeader = "CredentialHolderCountHeader";
        public const string Success = "Success";
        public const string Failed = "Failed";
        public const string Conflicted = "Conflicted";
        public const string Skipped = "Skipped";
        public const string Created = "Created";
        public const string Modified = "Modified";
        public const string Deleted = "Deleted";

        public const string CredentialHolderOldJson = "/kastle.com/OldData";
        public const string PlaiAgentVersionHeader = "PlaiAgentVersion";
        public const string IsTestSyncHeader = "IsTestSync";
        public const string IsMigratedADHeader = "IsMigratedAD";
    }
}

