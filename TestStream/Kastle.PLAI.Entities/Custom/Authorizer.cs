﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kastle.PLAI.Entities.Custom
{
    public class Authorizer
    {
        public int CardHolderId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public int CompanyId { get; set; }
        public string Email { get; set; }
        public int Flag { get; set; }
    }
}
