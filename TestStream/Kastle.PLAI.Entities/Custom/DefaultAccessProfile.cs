﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities
{
   public class DefaultAccessProfile
    {
        public string RoleGuid { get; set; }
        public string RoleName { get; set; }   
    }
}
