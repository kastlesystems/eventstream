﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class PublishStatusInfo
    {
        [XmlIgnore]
        public int ID { get; set; }

        [XmlElement("UID", Order = 0)]
        public string UID { get; set; }

        [XmlElement("CardHolderId", Order = 1)]
        public int CardHolderId { get; set; }

        [XmlElement("Status", Order = 2)]
        public string Status { get; set; }

        [XmlElement("State", Order = 3)]
        public string State { get; set; }

        [XmlIgnore]
        public string Xml { get; set; }
    }
}
