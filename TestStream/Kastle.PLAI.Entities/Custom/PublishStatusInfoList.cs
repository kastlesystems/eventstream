﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class PublishStatusInfoList
    {
        /// <summary>
        /// PublishStatusInfo
        /// </summary>
        [XmlElement("PublishStatusInfo", Order = 0)]
        public List<PublishStatusInfo> PublishStatusInfo { get; set; }
    }
}
