﻿using System;
using System.Collections.Generic;

namespace Kastle.PLAI.Entities.Custom
{
    [Serializable]
    public class SyncHistory
    {
        /// <summary>
        /// Gets or sets the synchronize unique identifier.
        /// </summary>
        /// <value>
        /// The synchronize unique identifier.
        /// </value>
        public string SyncGuid { get; set; }

        /// <summary>
        /// Gets or sets the agent version.
        /// </summary>
        /// <value>
        /// The agent version.
        /// </value>
        public string AgentVersion { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [automatic synchronize].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic synchronize]; otherwise, <c>false</c>.
        /// </value>
        public bool AutomaticSync { get; set; }

        /// <summary>
        /// Gets or sets the duration of the synchronize.
        /// </summary>
        /// <value>
        /// The duration of the synchronize.
        /// </value>
        public int SyncDuration { get; set; }

        /// <summary>
        /// Gets or sets the cardholder synchronize details.
        /// </summary>
        /// <value>
        /// The cardholder synchronize details.
        /// </value>
        public List<SyncDetails> CardholderSyncDetails { get; set; }

        /// <summary>
        /// Gets or sets the role synchronize details.
        /// </summary>
        /// <value>
        /// The role synchronize details.
        /// </value>
        public List<SyncDetails> RoleSyncDetails { get; set; }
        /// <summary>
        /// Gets or sets the address mapping mode.
        /// </summary>
        /// <value>
        /// The address mapping mode.
        /// </value>
        public string AddressMappingMode { get; set; }
    }

    [Serializable]
    public class SyncDetails
    {
        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the item status (Added, Modified or Deleted).
        /// </summary>
        public string ItemStatus { get; set; }

        /// <summary>
        /// Gets or sets the sync status (Success, Failed, Conflicted or Skipped).
        /// </summary>
        public string SyncPublishStatus { get; set; }
    }
}
