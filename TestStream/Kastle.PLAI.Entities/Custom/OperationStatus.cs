﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kastle.PLAI.Entities.Custom
{
    public class OperationStatus
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
