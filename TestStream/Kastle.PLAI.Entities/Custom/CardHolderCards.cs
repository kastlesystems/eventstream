﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.Custom
{
    public class CardHolderCards
    {
        [XmlElement("UID", Order = 0)]
        public string UID { get; set; }

        [XmlElement("Card", Order = 1)]
        public List<CardDetails> CardList { get; set; }
    }
}
