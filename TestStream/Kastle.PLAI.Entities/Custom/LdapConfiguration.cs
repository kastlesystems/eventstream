﻿using System;

namespace Kastle.PLAI.Entities
{
    public class LdapConfiguration
    {
        public Int64 Id { get; set; }

        
        public bool RunAutomatically { get; set; }

        
        public string LdapServer { get; set; }

        
        public string LdapPort { get; set; }

        
        public string LdapUserName { get; set; }

        
        public string LdapPassword { get; set; }

        
        public string UserSearchRoot { get; set; }

        
        public string UserFilter { get; set; }
        
        public string GroupSearchRoot { get; set; }

       
        public string GroupFilter { get; set; }

        
        public int AggregatorFrequency { get; set; }

       
        public bool IsSSLEnable { get; set; }

        
        public string ThresholdForSync { get; set; }

       
        public string DeleteThreshold { get; set; }

        
        public bool SyncBoth { get; set; }

        public bool IsFirstSyncAfterSyncBothUpdate { get; set; }

        public bool IsMigratedADMode { get; set; }
    }
}
