﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CSECOwnershipCookie Class for csec ownership cookie
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CSECOwnershipCookie
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the owner code.
        /// </summary>
        [XmlElement("ownerCode", Order = 0)]
        public string OwnerCode { get; set; }

        /// <summary>
        /// Gets or sets the expires.
        /// </summary>
        [XmlElement("expires", Order = 1)]
        public string Expires { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        [XmlElement("path", Order = 2)]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the cookie version.
        /// </summary>
        [XmlElement("cookieVersion", Order = 3)]
        public string CookieVersion { get; set; }

        /// <summary>
        /// Gets or sets the issuer signature.
        /// </summary>
        [XmlElement("issuerSignature", Order = 4)]
        public string IssuerSignature { get; set; }
        
    }
}
