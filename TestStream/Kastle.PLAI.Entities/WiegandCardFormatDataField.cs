﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.WiegandCardFormatDataField
    /// </summary>
    [XmlRoot("WiegandCardFormatDataField", Namespace = "urn:psialliance-org")]
    public class WiegandCardFormatDataField
    {
        [XmlElement("Name", Order = 0)]
        public string Name { get; set; }

        [XmlElement("Type", Order = 1)]
        public CardFormatDataFieldType Type { get; set; }

        [XmlElement("Offset", Order = 2)]
        public int Offset { get; set; }

        [XmlElement("Length", Order = 3)]
        public int Length { get; set; }

        [XmlElement("Value", Order = 4)]
        public int Value { get; set; }

        [XmlElement("ValueEncoding", Order = 5)]
        public IdentifierInfoValueEncoding ValueEncoding { get; set; }
    }

}