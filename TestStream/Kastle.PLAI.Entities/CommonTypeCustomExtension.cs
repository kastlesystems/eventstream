﻿using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CommonTypeCustomExtension Class for common type custom extension
    /// </summary>
    public class CommonTypeCustomExtension
    {
        /// <summary>
        /// Gets or sets the name of the custom extension.
        /// </summary>
        /// <value>
        /// The name of the custom extension.
        /// </value>
        [XmlElement(DataType = "anyURI")]
        public string CustomExtensionName { get; set; }

        /// <summary>
        /// Gets or sets any.
        /// </summary>
        [XmlAnyElementAttribute()]
        public XmlElement[] Any { get; set; }
    }
}
