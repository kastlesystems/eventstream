﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CSECOwnershipScope Class for csec ownership scope
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CSECOwnershipScope
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the permission descriptor list.
        /// </summary>
        [XmlElement("PermissionDescriptorList")]
        public PermissionDescriptorList PermissionDescriptorList { get; set; }
    }
}
