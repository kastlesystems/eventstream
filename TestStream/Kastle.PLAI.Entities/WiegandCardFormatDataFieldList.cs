﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.WiegandCardFormatDataFieldList
    /// </summary>
    [XmlRoot("WiegandCardFormatDataFieldList", Namespace = "urn:psialliance-org")]
    public class WiegandCardFormatDataFieldList
    {
        [XmlElement("WiegandCardFormatField")]
        public List<WiegandCardFormatField> WiegandCardFormatField { get; set; }
    }


    public class WiegandCardFormatField
    {
        [XmlElement("DataField")]
        public WiegandCardFormatDataField DataField { get; set; }

        [XmlElement("ParityField")]
        public WiegandCardFormatParityField ParityField { get; set; }
    }


    public class WiegandCardFormatParityField
    {
        [XmlElement("ParityType")]
        public WiegandCardFormatParityFieldType ParityType { get; set; }

        [XmlElement("CheckBitLocation")]
        public int CheckBitLocation { get; set; }

        [XmlElement("Start")]
        public int Start { get; set; }

        [XmlElement("Length")]
        public int Length { get; set; }

        [XmlElement("Mask")]
        public string Mask { get; set; }
    }

}
