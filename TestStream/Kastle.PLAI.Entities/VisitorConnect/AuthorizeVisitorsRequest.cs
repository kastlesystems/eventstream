﻿
using System.Collections.Generic;
using System.Data;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Class AuthorizeVisitorsRequest.
    /// </summary>

    public class AuthorizeVisitorsRequest
    {

        /// <summary>
        /// Gets or sets the visitors.
        /// </summary>
        /// <value>The visitors.</value>
        public List<VisitorDetails> Visitors { get; set; }
        public int CardHolderIdForIkastleUser { get; set; }
        public string TimeZoneUTCOffset
        {
            get;
            set;
        }

        public string TimeZoneName
        {
            get;
            set;
        }


        public string SuperUserName
        {
            get;
            set;
        }

        public string WatchListImagePath
        {
            get;
            set;
        }

        public bool IsFromImportVisitor
        {
            get;
            set;
        }

        public bool IsWatchListEnabled { get; set; }

        public DataTable WatchListInfo { get; set; }

    }
}
