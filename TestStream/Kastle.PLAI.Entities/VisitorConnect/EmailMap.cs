﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    public class EmailMap
    {
        [XmlAttribute]
        public string EmailID { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string NCID { get; set; }
    }
}