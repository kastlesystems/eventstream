﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    public class WatchListNotificationRequest
    {
        public VisitorDetails Visitor { get; set; }

        public List<IndividualDetail> watchList { get; set; }

        public string TimeZoneUTCOffset
        {
            get;
            set;
        }

        public string TimeZoneName
        {
            get;
            set;
        }

        public string SuperUserName
        {
            get;
            set;
        }

        public string WatchListImagePath
        {
            get;
            set;
        }

        public bool IsFromImportVisitor
        {
            get;
            set;
        }


    }
}
