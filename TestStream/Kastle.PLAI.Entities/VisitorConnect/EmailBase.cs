﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Enum Status
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// The pending
        /// </summary>
        Pending = 0,
        /// <summary>
        /// The success
        /// </summary>
        Success = 1,
        /// <summary>
        /// The failed
        /// </summary>
        Failed = 2,

    }

    /// <summary>
    /// Enum BodyFormat
    /// </summary>
    public enum BodyFormat
    {
        /// <summary>
        /// The text
        /// </summary>
        Text,
        /// <summary>
        /// The HTML
        /// </summary>
        HTML
    }

    /// <summary>
    /// Enum MailTemplate
    /// </summary>
    public enum MailTemplate
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The two factor mail template
        /// </summary>
        TwoFactorMailTemplate,

    }


    /// <summary>
    /// Class EmailBase.
    /// </summary>
    /// <summary>
    /// Class EmailBase.
    /// </summary>
    /// <summary>
    /// Class EmailBase.
    /// </summary>
    public class EmailBase
    {


        #region Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        /// <value>The updated by.</value>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated on.
        /// </summary>
        /// <value>The updated on.</value>
        public DateTime UpdatedOn { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>The created by.</value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>The created on.</value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the flags.
        /// </summary>
        /// <value>The flags.</value>
        public int Flags { get; set; }

        #endregion

    }
}
