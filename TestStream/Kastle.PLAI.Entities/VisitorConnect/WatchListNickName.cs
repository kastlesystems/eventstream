﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    [DataContract]
    [Serializable]
    public class WatchListNickName
    {
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string NickName
        {
            get;
            set;
        }
    }
}
