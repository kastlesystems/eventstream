﻿using System.Runtime.Serialization;
using System;
using System.Collections.Generic;

namespace Kastle.PLAI.Entities.VisitorConnect
{
   

        /// <summary>
        /// Class VisitorDetails.
        /// </summary>
        [DataContract]
        [Serializable]
        [KnownType(typeof(SpecialFloorSummary))]

        public partial class VisitorDetails
        {


            /// <summary>
            /// Gets or sets the identifier.
            /// </summary>
            /// <value>The identifier.</value>
            [DataMember]
            public int ID { get; set; }

            /// <summary>
            /// Gets or sets the last name.
            /// </summary>
            /// <value>The last name.</value>
            [DataMember]
            public string LastName
            {
                get;
                set;
            }

            [DataMember]
            public string EncryptedLastName
            {
                get;
                set;
            }
            /// <summary>
            /// Gets or sets the first name.
            /// </summary>
            /// <value>The first name.</value>
            [DataMember]
            public string FirstName
            {
                get;
                set;
            }
            [DataMember]
            public string EncryptedFirstName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the authorizer.
            /// </summary>
            /// <value>The authorizer.</value>
            [DataMember]
            public string Authorizer
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the start date.
            /// </summary>
            /// <value>The start date.</value>
            [DataMember]
            public string StartDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the end date.
            /// </summary>
            /// <value>The end date.</value>
            [DataMember]
            public string EndDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the last updated.
            /// </summary>
            /// <value>The last updated.</value>
            [DataMember]
            public string LastUpdated
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the updated by.
            /// </summary>
            /// <value>The updated by.</value>
            [DataMember]
            public string UpdatedBy
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the earliest time.
            /// </summary>
            /// <value>The earliest time.</value>
            [DataMember]
            public string EarliestTime
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the latest time.
            /// </summary>
            /// <value>The latest time.</value>
            [DataMember]
            public string LatestTime
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the company summary.
            /// </summary>
            /// <value>The company summary.</value>
            [DataMember]
            public CompanySummary CompanySummary
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the floor summary collection.
            /// </summary>
            /// <value>The floor summary collection.</value>
            [DataMember]
            public List<SpecialFloorSummary> FloorSummaryCollection
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the notes.
            /// </summary>
            /// <value>The notes.</value>
            [DataMember]
            public string Notes
            {
                get;
                set;
            }

            // Check In email id        
            /// <summary>
            /// Gets or sets the email.
            /// </summary>
            /// <value>The email.</value>
            [DataMember]
            public string Email
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the visitor email.
            /// </summary>
            /// <value>The visitor email.</value>
            [DataMember]
            public string VisitorEmail
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the special instruction.
            /// </summary>
            /// <value>The special instruction.</value>
            [DataMember]
            public string SpecialInstruction
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether [checked in].
            /// </summary>
            /// <value><c>null</c> if [checked in] contains no value, <c>true</c> if [checked in]; otherwise, <c>false</c>.</value>
            [DataMember]
            public bool? CheckedIn
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the checked in time.
            /// </summary>
            /// <value>The checked in time.</value>
            [DataMember]
            public string CheckedInTime
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the checked out time.
            /// </summary>
            /// <value>The checked out time.</value>
            [DataMember]
            public string CheckedOutTime
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the visitor company.
            /// </summary>
            /// <value>The visitor company.</value>
            [DataMember]
            public string VisitorCompany
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the visiting.
            /// </summary>
            /// <value>The visiting.</value>
            [DataMember]
            public string Visiting
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the day of week.
            /// </summary>
            /// <value>The day of week.</value>
            [DataMember]
            public string DayOfWeek
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the type of the frequency.
            /// </summary>
            /// <value>The type of the frequency.</value>
            [DataMember]
            public string FrequencyType
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the day number of month.
            /// </summary>
            /// <value>The day number of month.</value>
            [DataMember]
            public string DayNumberOfMonth
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the recurring after.
            /// </summary>
            /// <value>The recurring after.</value>
            [DataMember]
            public string RecurringAfter
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the day of month.
            /// </summary>
            /// <value>The day of month.</value>
            [DataMember]
            public string DayOfMonth
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the month of year.
            /// </summary>
            /// <value>The month of year.</value>
            [DataMember]
            public string MonthOfYear
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is from mobile application.
            /// </summary>
            /// <value><c>null</c> if [is from mobile application] contains no value, <c>true</c> if [is from mobile application]; otherwise, <c>false</c>.</value>
            [DataMember]
            public bool? IsFromMobileApp
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets any to flag.
            /// </summary>
            /// <value>Any to flag.</value>
            [DataMember]
            public int AnyToFlag { get; set; }

            /// <summary>
            /// Gets or sets the security token.
            /// </summary>
            /// <value>The security token.</value>
            [DataMember]
            public Guid SecurityToken { get; set; }

            [DataMember]
            public string OnBehalfOF
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the on behalf of.
            /// </summary>
            /// <value>The on behalf of.</value>
            [DataMember]
            public string ErrorDetails
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the error details.
            /// </summary>
            /// <value>The error details.</value>
            [DataMember]
            public bool IsError
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is error.
            /// </summary>
            /// <value><c>true</c> if this instance is error; otherwise, <c>false</c>.</value>
            [DataMember]
            public int VendorExpirationDays
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the vendor expiration days.
            /// </summary>
            /// <value>The vendor expiration days.</value>
            [DataMember]
            public string VendorNotifyEmailList
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the vendor notify email list.
            /// </summary>
            /// <value>The vendor notify email list.</value>
            [DataMember]
            public bool IsVisitorVendor
            {
                get;
                set;
            }
            /// <summary>
            /// Gets or sets the Enterprise Logo.
            /// </summary>
            /// <value>The Enterprise Logo</value>
            [DataMember]
            public string EnterpriseLogo
            {
                get;
                set;
            }
            /// <summary>
            /// Gets or sets the Authorizer email.
            /// </summary>
            /// <value>The Authorizer email.</value>
            [DataMember]
            public string HostEmail { get; set; }

            /// <summary>
            /// Gets or sets the Enterprise Id.
            /// </summary>
            /// <value>The Enterprise ID</value>
            [DataMember]
            public Int32 EnterpriseId
            {
                get;
                set;
            }

            [DataMember]
            public bool IsScheduleMailSendToVisitor
            {
                get;
                set;
            }
        [DataMember]
        public int AuthorizerId { get; set; }

    }
    
}
