﻿using System.Runtime.Serialization;
using System;
using System.Collections.Generic;

namespace Kastle.PLAI.Entities.VisitorConnect
{
    /// <summary>
    /// Class CompanySummary.
    /// </summary>
    [DataContract]
    [Serializable]
    public partial class CompanySummary
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [DataMember]
        public int? ID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is full edit.
        /// </summary>
        /// <value><c>true</c> if this instance is full edit; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsFullEdit { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the company.
        /// </summary>
        /// <value>The type of the company.</value>
        [DataMember]
        public string CompanyType { get; set; }

        /// <summary>
        /// Gets or sets the building identifier.
        /// </summary>
        /// <value>The building identifier.</value>
        [DataMember]
        public int BuildingID { get; set; }

        /// <summary>
        /// Gets or sets the parent company identifier.
        /// </summary>
        /// <value>The parent company identifier.</value>
        [DataMember]
        public int? ParentCompanyID { get; set; }

        /// <summary>
        /// Gets or sets the parent building identifier.
        /// </summary>
        /// <value>The parent building identifier.</value>
        [DataMember]
        public int ParentBuildingID { get; set; }

        /// <summary>
        /// Gets or sets the building address.
        /// </summary>
        /// <value>The building address.</value>
        [DataMember]
        public string BuildingAddress { get; set; }

        /// <summary>
        /// Gets or sets the building number.
        /// </summary>
        /// <value>The building number.</value>
        [DataMember]
        public string BuildingNumber { get; set; }

        /// <summary>
        /// Gets or sets the parent building number.
        /// </summary>
        /// <value>The parent building number.</value>
        [DataMember]
        public string ParentBuildingNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the access profile.
        /// </summary>
        /// <value>The type of the access profile.</value>
        [DataMember]
        public AccessProfileType AccessProfileType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [two factor authentication required].
        /// </summary>
        /// <value><c>true</c> if [two factor authentication required]; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool TwoFactorAuthenticationRequired { get; set; }

        /// <summary>
        /// Gets or sets the managed by company.
        /// </summary>
        /// <value>The managed by company.</value>
        [DataMember]
        public int? ManagedByCompany { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is owner.
        /// </summary>
        /// <value><c>true</c> if this instance is owner; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsOwner { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is schedule enabled.
        /// </summary>
        /// <value><c>true</c> if this instance is schedule enabled; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsScheduleEnabled { get; set; }


        //Enterprise Fields
        [DataMember]
        public string BuildingName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in scope subsidiary.
        /// </summary>
        /// <value><c>true</c> if this instance is in scope subsidiary; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsInScopeSubsidiary { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is management company.
        /// </summary>
        /// <value><c>true</c> if this instance is management company; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsManagementCompany { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is checked.
        /// </summary>
        /// <value><c>true</c> if this instance is checked; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsChecked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is part of enterprise.
        /// </summary>
        /// <value><c>true</c> if this instance is part of enterprise; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsPartOfEnterprise { get; set; }

        /// <summary>
        /// Gets or sets the name of the parent.
        /// </summary>
        /// <value>The name of the parent.</value>
        [DataMember]
        public string ParentName { get; set; }

        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        /// <value>The flag.</value>
        [DataMember]
        public string Flag { get; set; }

        /// <summary>
        /// Gets or sets the short name.
        /// </summary>
        /// <value>The short name.</value>
        [DataMember]
        public string ShortName { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is ble enabled.
        /// </summary>
        /// <value><c>true</c> if this instance is ble enabled; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsBleEnabled { get; set; }

        /// <summary>
        /// Gets or sets the floor plan ids.
        /// </summary>
        /// <value>The floor plan ids.</value>
        [DataMember]
        public string FloorPlanIds { get; set; }

        /// <summary>
        /// Gets or sets to company identifier.
        /// </summary>
        /// <value>To company identifier.</value>
        [DataMember]
        public int ToCompanyId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [only access profile assignment].
        /// </summary>
        /// <value><c>true</c> if [only access profile assignment]; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool OnlyAccessProfileAssignment { get; set; }

        // represents company is managed by the authorizer
        [DataMember]
        public bool IsManagedCompany { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is ap only.
        /// </summary>
        /// <value><c>true</c> if this instance is ap only; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsAPOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is shared.
        /// </summary>
        /// <value><c>true</c> if this instance is shared; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsShared { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in authorizer scope.
        /// </summary>
        /// <value><c>true</c> if this instance is in authorizer scope; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsInAuthorizerScope { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is delegated rg comp.
        /// </summary>
        /// <value><c>true</c> if this instance is delegated rg comp; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsDelegatedRGComp { get; set; }

        //Added for mass Import
        [DataMember]
        public string DefaultStartTime { get; set; }

        /// <summary>
        /// Gets or sets the default end time.
        /// </summary>
        /// <value>The default end time.</value>
        [DataMember]
        public string DefaultEndTime { get; set; }

        /// <summary>
        /// Gets or sets the maximum duration.
        /// </summary>
        /// <value>The maximum duration.</value>
        [DataMember]
        public int MaxDuration { get; set; }

        /// <summary>
        /// Gets or sets the default floor identifier.
        /// </summary>
        /// <value>The default floor identifier.</value>
        [DataMember]
        public string DefaultFloorId { get; set; }

        /// <summary>
        /// Gets or sets the company location.
        /// </summary>
        /// <value>The company location.</value>
        [DataMember]
        public string CompanyLocation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is building qr configurable.
        /// </summary>
        /// <value><c>true</c> if this instance is building qr configurable; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsBuildingQRConfigurable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is company visitor mail configured.
        /// </summary>
        /// <value><c>true</c> if this instance is company visitor mail configured; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsCompanyVisitorMailConfigured { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is anonymity enable
        /// </summary>
        /// <value><c>true</c> if this instance is anonymity enable; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsAnonymityEnable { get; set; }

        [DataMember]
        public List<EncryptionDetails> EncryptionDetails { get; set; }


        /// <summary>
        /// Gets value indicating whether this company is configured for send an hour prior scheduled mail.
        /// </summary>
        /// <value><c>true</c> if this company is configured; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsSendPreScheduledMail { get; set; }

        /// <summary>
        /// gets or sets the building timezone offset
        /// </summary>
        [DataMember]
        public int BuilidingOffset { get; set; }

        /// <summary>
        /// gets or sets the Is schedule subscribed
        /// </summary>
        [DataMember]
        public bool IsScheduleSubscribed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is PLAI institution.
        /// </summary>
        /// <value><c>true</c> if this is PLAI institution; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsPLAIInst { get; set; }

        /// <summary>
        /// Gets or sets the Company Instructions.
        /// </summary>
        /// <value>The Company Instructions.</value>
        [DataMember]
        public string CompanyInstructions { get; set; }
        /// <summary>
        /// Gets or sets the Building Instructions.
        /// </summary>
        /// <value>The Building Instructions.</value>
        [DataMember]
        public string BuildingInstructions { get; set; }

        /// <summary>
        /// Gets or sets the IsRVMSubscribed
        /// </summary>
        /// <value>The IsRVMSubscribed.</value>
        [DataMember]
        public bool IsRVMSubscribed { get; set; }

        /// <summary>
        /// Gets or sets the IsCommunity
        /// </summary>
        /// <value>The IsCommunity.</value>
        [DataMember]
        public bool IsCommunity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is KP Subscribed.
        /// </summary>
        /// <value><c>true</c> if this instance is KP Subscribed; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsKPSubscribed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is Bldg Hour Offset.
        /// </summary>
        /// <value><c>true</c> if this instance is Bldg Hour Offset; otherwise, <c>false</c>.</value>
        [DataMember]
        public int BldgHourOffset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is Bldg Hour Offset.
        /// </summary>
        /// <value><c>true</c> if this instance is Bldg Hour Offset; otherwise, <c>false</c>.</value>
        [DataMember]
        public int ServerOffset { get; set; }

        [DataMember]
        public bool IsSsoEnabled { get; set; }

        [DataMember]
        public List<string> TrustedDomains
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTower { get; set; }

        [DataMember]
        public bool IsDualPinSubscribed { get; set; }

        [DataMember]
        public bool IsCrossDomainManagedByCompany { get; set; }

        [DataMember]
        public bool IsCrossDomainBranchCompany { get; set; }

        [DataMember]
        public bool IsLeasingOffice { get; set; }

        [DataMember]
        public bool IsNameFieldDisabled { get; set; }
        [DataMember]
        public bool IsKPKTenant { get; set; }
        [DataMember]
        public List<string> GarageGuidList { get; set; }
        [DataMember]
        public string TenantGuidId { get; set; }
        [DataMember]
        public bool IsKPOP { get; set; }
        [DataMember]
        public string BuildingTimeZone { get; set; }

    }
}
