﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kastle.PLAI.Entities.VisitorConnect
{

    /// <summary>
    /// Class ImageInfo.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ImageInfo
    {
        /// <summary>
        /// Gets or sets the image BLOB.
        /// </summary>
        /// <value>The image BLOB.</value>
        [DataMember]
        public string ImageBlob
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the image extension.
        /// </summary>
        /// <value>The image extension.</value>
        [DataMember]
        public string ImageExtension
        {
            get;
            set;
        }
    }
}
