﻿using System.Runtime.Serialization;
using System;


namespace Kastle.PLAI.Entities.VisitorConnect
{
    [DataContract]
    [Serializable]
    public class EncryptionDetails
    {

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public string Password
        {
            get;
            set;
        }

        [DataMember]
        public string StartDate
        {
            get;
            set;
        }


        [DataMember]
        public string EndDate
        {
            get;
            set;
        }

    }
}
