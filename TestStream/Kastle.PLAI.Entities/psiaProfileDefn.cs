﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PsiaProfileDefn Class for psia profile defn
    /// </summary>
    public class PsiaProfileDefn
    {
        /// <summary>
        /// Gets or sets the name of the psia profile.
        /// </summary>
        /// <value>
        /// The name of the psia profile.
        /// </value>
        [XmlElement("psiaProfileName", Order = 0)]
        public string PsiaProfileName { get; set; }

        /// <summary>
        /// Gets or sets the psia profile version.
        /// </summary>
        [XmlElement("psiaProfileVersion", Order = 1)]
        public float PsiaProfileVersion { get; set; }

        /// <summary>
        /// Gets or sets the psia spec.
        /// </summary>
        [XmlElement("psiaSpec", Order = 2)]
        public PsiaSpecTag PsiaSpec { get; set; }
    }
}
