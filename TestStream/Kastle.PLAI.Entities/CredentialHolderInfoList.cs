﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CredentialHolderInfoList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the credential holder information.
        /// </summary>
        [XmlElement("CredentialHolderInfo")]
        public List<CredentialHolderInfo> CredentialHolderInfo { get; set; }
    }
}
