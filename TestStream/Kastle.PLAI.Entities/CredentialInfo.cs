﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialInfo Class for credential information
    /// </summary>
    [XmlRoot("CredentialInfo", Namespace = "urn:psialliance-org")]
    public class CredentialInfo
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("ID", Order = 0)]
        public uint ID { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [XmlElement("UID", Order = 1)]
        public string UID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("Name", Order = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlElement("Description", Order = 3)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the source uid.
        /// </summary>
        [XmlElement("SourceUID", Order = 4)]
        public string SourceUID { get; set; }

        /// <summary>
        /// Gets or sets the assigned to identifier.
        /// </summary>
        [XmlElement("AssignedToID", Order = 5)]
        public ReferenceID AssignedToID { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        [XmlElement("State", Order = 6)]
        public StateOfCredential State { get; set; }

        /// <summary>
        /// Gets or sets the valid from.
        /// </summary>
        [XmlElement("ValidFrom", Order = 7)]
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [valid from specified].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid from specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ValidFromSpecified { get; set; }

        /// <summary>
        /// Gets or sets the valid to.
        /// </summary>
        [XmlElement("ValidTo", Order = 8)]
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [valid to specified].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid to specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ValidToSpecified { get; set; }

        /// <summary>
        /// Gets or sets the identifier information list.
        /// </summary>
        [XmlArray("IdentifierInfoList", Order = 9)]
        [XmlArrayItem("IdentifierInfo")]
        public List<IdentifierInfo> IdentifierInfoList { get; set; }

        /// <summary>
        /// Gets or sets the permission identifier list.
        /// </summary>
        [XmlElement("PermissionIDList", Order = 10)]
        public PermissionIDList PermissionIDList { get; set; }

        /// <summary>
        /// Gets or sets a value for MeetingInfoId.
        /// </summary>
        /// <value>
        /// </value>
        //[XmlIgnore]
        [XmlElement("MeetingInfoUID", Order = 11)]
        public string MeetingInfoUID { get; set; }

    }
}
