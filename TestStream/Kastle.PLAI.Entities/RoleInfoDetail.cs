﻿using System.Collections.Generic;
using System.Xml.Serialization;
namespace Kastle.PLAI.Entities
{
    [XmlRoot("RoleInfoDetail", Namespace = "urn:psialliance-org")]
    public class RoleInfoDetail: RoleInfo
    {

        /// <summary>
        /// Gets or sets the Flag.
        /// </summary>
        [XmlElement("Flag", Order = 5)]
        public int Flag { get; set; }
    }
    public class RoleInfoDetailList
    {
        public List<RoleInfoDetail> RolesInfoDetail { get; set; }
    }
}
