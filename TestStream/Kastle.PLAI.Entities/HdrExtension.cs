﻿using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.HdrExtension Class for HDR extension
    /// </summary>
    public class HdrExtension
    {
        /// <summary>
        /// Gets or sets the name of the extension.
        /// </summary>
        /// <value>
        /// The name of the extension.
        /// </value>
        [XmlElement(DataType = "anyURI")]
        public string ExtensionName { get; set; }

        /// <summary>
        /// Gets or sets any.
        /// </summary>
        [XmlAnyElementAttribute()]
        public XmlElement[] Any { get; set; }
    }
}
