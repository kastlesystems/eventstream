﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PermissionDescriptorList Class for permission descriptor list
    /// </summary>
    public class PermissionDescriptorList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the permission descriptor.
        /// </summary>
        [XmlElement("PermissionDescriptor")]
        public List<PermissionDescriptor> PermissionDescriptor { get; set; }
    }
}
