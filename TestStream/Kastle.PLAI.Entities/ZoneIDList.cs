﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.ZoneIDList Class for zone identifier list
    /// </summary>
    public class ZoneIDList
    {
        /// <summary>
        /// Gets or sets the zone identifier.
        /// </summary>
        [XmlElement("ZoneID")]
        public ReferenceID[] ZoneID { get; set; }
    }
}
