﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PsiaSpecDecl Class for psia spec decl
    /// </summary>
    public class PsiaSpecDecl
    {
        /// <summary>
        /// Gets or sets the name of the psia spec.
        /// </summary>
        /// <value>
        /// The name of the psia spec.
        /// </value>
        [XmlElement("psiaSpecName", Order = 0)]
        public PsiaSpecTag PsiaSpecName { get; set; }

        /// <summary>
        /// Gets or sets the psia spec version.
        /// </summary>
        [XmlElement("psiaSpecVersion", Order = 1)]
        public float PsiaSpecVersion { get; set; }

        /// <summary>
        /// Gets or sets the psia spec profile.
        /// </summary>
        [XmlElement("psiaSpecProfile", Order = 2)]
        public PsiaSpecProfileLevel PsiaSpecProfile { get; set; }

        /// <summary>
        /// Gets or sets the psia spec information.
        /// </summary>
        [XmlElement("psiaSpecInfo", Order = 3)]
        public string PsiaSpecInfo { get; set; }
    }
}
