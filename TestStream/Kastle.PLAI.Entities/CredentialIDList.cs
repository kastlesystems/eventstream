﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialIDList Class for credential identifier list
    /// </summary>
    public class CredentialIDList
    {
        /// <summary>
        /// Gets or sets the credential identifier.
        /// </summary>
        [XmlElement("CredentialID")]
        public List<ReferenceID> CredentialID { get; set; }
    }
}
