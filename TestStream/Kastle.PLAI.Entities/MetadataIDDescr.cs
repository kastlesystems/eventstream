﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.MetadataIDDescr Class for metadata identifier description
    /// </summary>
    public class MetadataIDDescr
    {
        /// <summary>
        /// Gets or sets the metdata mids.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified, DataType = "anyURI")]
        public string metdataMIDS { get; set; }

        /// <summary>
        /// Gets or sets the metadata priority.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public byte metadataPriority { get; set; }

        /// <summary>
        /// Gets or sets the metadata item mode.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public ParamModMode metadataItemMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [metadata item mode specified].
        /// </summary>
        /// <value>
        /// <c>true</c> if [metadata item mode specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnoreAttribute()]
        public bool metadataItemModeSpecified { get; set; }
    }
}
