﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    public class MetadataList
    {
        /// <summary>
        /// Gets or sets the number of entries.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public uint numOfEntries { get; set; }

        /// <summary>
        /// Gets or sets the metadata description list.
        /// </summary>
        [XmlArray(Form = XmlSchemaForm.Unqualified), XmlArrayItemAttribute("metadataDescriptor", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public MetadataDescriptor[] metadataDescrList { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }
    }
}
