﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.PermissionDescriptor Class for permission descriptor
    /// </summary>
    public class PermissionDescriptor
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("id", Order = 0)]
        public uint Id { get; set; }

        /// <summary>
        /// Gets or sets the symbolic permission descriptor.
        /// </summary>
        [XmlElement("SymbolicPermissionDescriptor", Order = 1)]
        public string SymbolicPermissionDescriptor { get; set; }
    }
}
