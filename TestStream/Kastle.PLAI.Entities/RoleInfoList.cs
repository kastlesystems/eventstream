﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.RoleInfoList Class for role information list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class RoleInfoList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the roles information.
        /// </summary>
        [XmlElement("RoleInfo")]
        public List<RoleInfo> RolesInfo { get; set; }
    }
}
