﻿using System;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.DeviceStatus Class for device status
    /// </summary>
    [XmlRoot("DeviceStatus", Namespace = "urn:psialliance-org")]
    public class DeviceStatus
    {
        /// <summary>
        /// version
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }
        
        /// <summary>
        /// currentDeviceTime
        /// </summary>
        [XmlElement("currentDeviceTime")]
        public DateTime currentDeviceTime { get; set; }
    }
}

