﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.RoleInfo Class for role information
    /// </summary>
    [XmlRoot("RoleInfo", Namespace = "urn:psialliance-org")]
    public class RoleInfo
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [XmlElement("ID", Order = 0)]
        public uint Id { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [XmlElement("UID", Order = 1)]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("Name", Order = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlElement("Description", Order = 3)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the permission identifier list.
        /// </summary>
        [XmlElement("PermissionIDList", Order = 4)]
        public PermissionIDList PermissionIDList { get; set; }

        

    } 
}
