﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.CredentialHolderCredentialList Class for credential holder credentials list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class CredentialHolderCredentialList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the credential holder credentials.
        /// </summary>
        [XmlElement("CredentialHolderCredentials")]
        public List<CredentialHolderCredentials> CredentialHolderCredentials { get; set; }
    }
}
