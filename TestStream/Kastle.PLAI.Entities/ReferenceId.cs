﻿using System.Xml;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.ReferenceID Class for reference identifier
    /// </summary>
    public class ReferenceID
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public uint ID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [identifier specified].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [identifier specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore()]
        public bool IDSpecified { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public string GUID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the custom extension.
        /// </summary>
        [XmlElement("CustomExtension")]
        public CommonTypeCustomExtension[] CustomExtension { get; set; }

        /// <summary>
        /// Gets or sets any attribute.
        /// </summary>
        [XmlAnyAttributeAttribute()]
        public XmlAttribute[] AnyAttr { get; set; }
    }
}
