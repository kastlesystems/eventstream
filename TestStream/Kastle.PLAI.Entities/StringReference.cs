﻿namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.StringReference Class for string reference
    /// </summary>
    public class StringReference
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        public string Number { get; set; }
    }
}
