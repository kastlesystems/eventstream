﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.AreaControlEvent Class for area control event
    /// </summary>
    [XmlRoot("AreaControlEvent", Namespace = "urn:psialliance-org")]
    public class AreaControlEvent
    {
        /// <summary>
        /// Gets or sets the metadata header.
        /// </summary>
        public MetaHeader MetadataHeader { get; set; }

        /// <summary>
        /// Gets or sets the event data.
        /// </summary>
        public EventData EventData { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttributeAttribute()]
        public string version { get; set; }
    }
}
