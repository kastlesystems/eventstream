﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.DeviceCertificateList Class for device certificate list
    /// </summary>
    [XmlRoot("", Namespace = "urn:psialliance-org")]
    public class DeviceCertificateList
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the device certificates.
        /// </summary>
        [XmlElement("DeviceCertificateList")]
        public List<DeviceCertificate> DeviceCertificates { get; set; }
    }
}
