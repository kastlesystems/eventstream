﻿using System.Xml.Serialization;

namespace Kastle.PLAI.Entities
{
    /// <summary>
    /// Fully qualified name :  Kastle.PLAI.Entities.EventValueState Class for event value state
    /// </summary>
    public class EventValueState
    {
        /// <summary>
        /// Gets or sets the item.
        /// Item can be any one of the below types:
        /// 1. CredentialHolderID of type ReferenceID
        /// 2. Denied of type AccessDeniedReason
        /// 3. Granted of type AccessGrantedReason
        /// 4. Requested of type CredentialInfo
        /// 5. value of type float
        /// 
        /// </summary>
        [XmlElement("CredentialHolderID", typeof(ReferenceID))]
        [XmlElement("Denied", typeof(AccessDeniedReason))]
        [XmlElement("Granted", typeof(AccessGrantedReason))]
        [XmlElement("Requested", typeof(CredentialInfo))]
        [XmlElement("Value", typeof(float))]
        public object Item { get; set; }
    }
}